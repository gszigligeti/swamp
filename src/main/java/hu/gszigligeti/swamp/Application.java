package hu.gszigligeti.swamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        // Entry point of application
        SpringApplication.run(Application.class, args);
    }
}
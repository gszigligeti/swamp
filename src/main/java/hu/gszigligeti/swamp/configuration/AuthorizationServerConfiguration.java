package hu.gszigligeti.swamp.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * Configuration class containing every OAuth authorization setting
 * @author Gergely Szigligeti
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {
    @Autowired
    @Qualifier("SwampAuthenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Configure the security of the Authorization Server (practically the /oauth/token endpoint)
     * @param security Injected bean provided by Spring Security for customization
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        // Do not require Basic authentication for token requests
        security.allowFormAuthenticationForClients();
    }

    /**
     * Configure the OAuth clients of the application
     * @param clients Injected bean provided by Spring Security for customization
     * @throws Exception
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        InMemoryClientDetailsServiceBuilder clientDetailsServiceBuilder = new InMemoryClientDetailsServiceBuilder();

        // Configure the Web and Android public clients for OAuth
        clientDetailsServiceBuilder.withClient("077BB83D76FD811663CE").scopes("swamp_web").authorizedGrantTypes("password", "refresh_token");
        clientDetailsServiceBuilder.withClient("89EF884202D198CD213D").scopes("swamp_android").authorizedGrantTypes("password", "refresh_token");

        clients.withClientDetails(clientDetailsServiceBuilder.build());
    }

    /**
     * Configure the non-security features of the Authorization Server endpoints
     * @param endpoints Injected bean provided by Spring Security for customization
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(this.authenticationManager).userDetailsService(this.userDetailsService).tokenStore(new InMemoryTokenStore());

        // Configure OAuth endpoints (just for consistency purposes)
        endpoints.pathMapping("/oauth/authorize", "/api/authentication/authorize");
        endpoints.pathMapping("/oauth/token", "/api/authentication/token");
        endpoints.pathMapping("/oauth/check_token", "/api/authentication/check_token");
        endpoints.pathMapping("/oauth/confirm_access", "/api/authentication/confirm_access");
        endpoints.pathMapping("/oauth/error", "/api/authentication/error");
        endpoints.pathMapping("/oauth/token_key", "/api/authentication/token_key");
    }
}
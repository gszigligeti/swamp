package hu.gszigligeti.swamp.configuration;

import hu.gszigligeti.swamp.utilities.SwampLogoutSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Configuration class containing every resource server setting
 * @author Gergely Szigligeti
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    @Autowired
    private SwampLogoutSuccessHandler logoutSuccessHandler;

    /**
     * Method for initializing the HTTP protocol security on the resources
     * @param http Injected bean provided by Spring Security for customization
     * @throws Exception General exception for authorization errors
     */
    @Override
    public void configure(HttpSecurity http) throws Exception {
        // Disable CSRF and X-FRAME-OPTIONS protection, since we are using stateless authentication
        http.csrf().disable().headers().frameOptions().disable();
        http.logout().logoutUrl("/api/authentication/logout").logoutSuccessHandler(logoutSuccessHandler);

        // Secure protected templates
        http.authorizeRequests().antMatchers("/templates/protected/**").authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/api/authentication/me").authenticated();
        http.authorizeRequests().antMatchers("/api/authentication/**").permitAll();
        http.authorizeRequests().antMatchers("/api/**").authenticated();
    }
}
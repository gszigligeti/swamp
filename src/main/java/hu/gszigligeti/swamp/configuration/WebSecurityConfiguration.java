package hu.gszigligeti.swamp.configuration;

import hu.gszigligeti.swamp.constants.SwampApplicationProfiles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Configuration class containing every security-related setting
 * @author Gergely Szigligeti
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * Method for building the authentication manager (data source, password hashing, etc.)
     * @param auth Injected bean provided by Spring Security for customization
     * @throws Exception General exception for authentication errors
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userDetailsService).passwordEncoder(securePasswordEncoder());
    }

    /**
     * Method for setting up the secured and non-secured web resources
     * @param web Injected bean provided by Spring Security for customization
     * @throws Exception General exception for authorization errors
     */
    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
    }

    /**
     * Method for initializing the HTTP protocol security (sessions, urls, etc.)
     * @param http Injected bean provided by Spring Security for customization
     * @throws Exception General exception for authorization errors
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.authorizeRequests().antMatchers("/api/authentication/authorize").authenticated();
    }

    /**
     * Secure password encoder, which uses the BCrypt algorithm to salt and hash passwords
     * @return The bean of the instantiated password encoder
     */
    @Bean
    public PasswordEncoder securePasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * Method for setting the welcome page properly to the development profile
     * @return The configured adapter that resolves the root path to index_development.html
     */
    @Bean
    @Profile(SwampApplicationProfiles.DEVELOPMENT)
    public WebMvcConfigurerAdapter forwardToDevelopmentPage() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("forward:/index_development.html");
            }
        };
    }

    /**
     * Method for setting the welcome page properly to the production profile
     * @return The configured adapter that resolves the root path to index_production.html
     */
    @Bean
    @Profile(SwampApplicationProfiles.PRODUCTION)
    public WebMvcConfigurerAdapter forwardToProductionPage() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("forward:/index_production.html");
            }
        };
    }

    /**
     * Expose the configured authentication manager for OAuth as a bean
     * @return The AuthenticationManager instance that we have configured in the first method
     */
    @Override
    @Bean(name = "SwampAuthenticationManager")
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
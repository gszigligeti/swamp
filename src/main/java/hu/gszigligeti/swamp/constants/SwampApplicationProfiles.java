package hu.gszigligeti.swamp.constants;

/**
 * Constant values for application profiles
 * @author Gergely Szigligeti
 */
public final class SwampApplicationProfiles {
    public static final String DEVELOPMENT = "development";
    public static final String PRODUCTION = "production";
}
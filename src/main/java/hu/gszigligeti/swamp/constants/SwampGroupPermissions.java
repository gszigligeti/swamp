package hu.gszigligeti.swamp.constants;

import java.io.Serializable;

import static hu.gszigligeti.swamp.constants.SwampGroupRoles.ADMINISTRATOR;
import static hu.gszigligeti.swamp.constants.SwampGroupRoles.EDITOR;
import static hu.gszigligeti.swamp.constants.SwampGroupRoles.MEMBER;
import static hu.gszigligeti.swamp.constants.SwampGroupRoles.OWNER;

public final class SwampGroupPermissions implements Serializable {
    public static final byte READ = MEMBER | EDITOR | ADMINISTRATOR | OWNER;
    public static final byte WRITE = EDITOR | ADMINISTRATOR | OWNER;
    public static final byte PRIVILEGED = ADMINISTRATOR | OWNER;
    public static final byte SUPERUSER = OWNER;
}
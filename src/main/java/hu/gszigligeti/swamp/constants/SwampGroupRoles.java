package hu.gszigligeti.swamp.constants;

import java.io.Serializable;

/**
 * Constant values for possible roles within a group
 * @author Gergely Szigligeti
 */
public final class SwampGroupRoles implements Serializable {
    public static final byte MEMBER         = 0b0001;
    public static final byte EDITOR         = 0b0010;
    public static final byte ADMINISTRATOR  = 0b0100;
    public static final byte OWNER          = 0b1000;
}
package hu.gszigligeti.swamp.constants;

import java.io.Serializable;

/**
 * Constant values for possible types of different pockets
 * @author Gergely Szigligeti
 */
public final class SwampPocketTypes implements Serializable {
    public static final String USER = "USER";
    public static final String GROUP = "GROUP";
}
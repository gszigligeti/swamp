package hu.gszigligeti.swamp.constants;

import java.io.Serializable;

/**
 * Constant values for possible roles within the application
 * @author Gergely Szigligeti
 */
public final class SwampUserRoles implements Serializable {
    public static final String USER = "USER";
    public static final String ADMINISTRATOR = "ADMIN";
}
package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampAccountDetails;
import hu.gszigligeti.swamp.controllers.dto.SwampRegistrationRequest;
import hu.gszigligeti.swamp.exceptions.SwampRegistrationException;
import hu.gszigligeti.swamp.exceptions.SwampUserNotFoundException;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

/**
 * Controller class for every authentication-related operation
 * @author Gergely Szigligeti
 */
@RestController
@RequestMapping("/api/authentication")
public class SwampAuthenticationController {
    @Autowired
    private SwampUserService userService;

    /**
     * This method is responsible for registering a new user in the system
     * @param registrationRequest The Java-representation of the incoming JSON object
     * @param request The HTTP request injected by Spring
     * @return HTTP 201 Created if the user has been created, otherwise HTTP 500 Internal Server Error
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerSwampUser(@RequestBody @Valid final SwampRegistrationRequest registrationRequest, HttpServletRequest request) {
        final SwampUser registeredUser = this.userService.registerNewUser(registrationRequest, request);
        return new ResponseEntity(registeredUser == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.CREATED);
    }

    /**
     * This method is responsible for providing the frontend to check if the user is authenticated
     * @return The currently logged in user's identifier
     */
    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    public ResponseEntity checkUserAuthentication() {
        return new ResponseEntity<SwampAccountDetails>(new SwampAccountDetails(this.userService.getCurrentlyAuthenticatedUser()), HttpStatus.OK);
    }

    /**
     * This method is responsible for activating a brand new Swamp account
     * @param activationCode The activation code as a GET parameter
     * @return HTTP 200 OK if the user has been activated, otherwise HTTP 404 Not found (there's no user with this code)
     */
    @RequestMapping(value = "/activate", method = RequestMethod.POST)
    public ResponseEntity activateSwampUserAccount(@RequestParam("code") String activationCode) {
        final SwampUser activatedUser = this.userService.activateUserAccount(activationCode);
        return new ResponseEntity(activatedUser == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for sending the password reset email if the user can't remember it
     * @param emailAddress The email address of the user
     * @return HTTP 200 OK if the email has been sent, otherwise HTTP 404 Not found (there's no user with this email)
     */
    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public ResponseEntity sendPasswordResetEmail(@RequestParam("email") String emailAddress, HttpServletRequest servletRequest) {
        final SwampUser updatedUser = this.userService.sendPasswordResetEmail(emailAddress, servletRequest);
        return new ResponseEntity(updatedUser == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating a user's password if they can't remember it
     * @param token The secret token that has been sent to the user via email
     * @param password The new password that the user wants to setup
     * @return HTTP 200 OK if the password has been updated, otherwise HTTP 404 Not found (there's no user with this token)
     */
    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public ResponseEntity resetForgottenPassword(@RequestParam String token, @RequestParam String password) {
        final SwampUser updatedUser = this.userService.resetForgottenPassword(token, password);
        return new ResponseEntity(updatedUser == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    // ~~~~ EXCEPTION HANDLERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Exception handler method for registration exceptions like existing emails, etc.
     * @param exception The exception object injected by Spring
     * @return HTTP 406 Not Acceptable
     */
    @ExceptionHandler(SwampRegistrationException.class)
    public ResponseEntity handleRegistrationErrors(SwampRegistrationException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.NOT_ACCEPTABLE);
    }

    /**
     * Exception handler method for handling if the user could not be found based on the provided details
     * @param exception The exception object injected by Spring
     * @return HTTP 404 Not found
     */
    @ExceptionHandler(SwampUserNotFoundException.class)
    public ResponseEntity handleInvalidAccountDetails(SwampUserNotFoundException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    /**
     * Exception handler method for email sending problems (unavailable service, SMTP errors, etc.)
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(MessagingException.class)
    public ResponseEntity handleEmailSendingErrors(MessagingException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", "MAIL_ERROR"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Exception handler method for generic runtime exceptions
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleGenericRuntimeException(RuntimeException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
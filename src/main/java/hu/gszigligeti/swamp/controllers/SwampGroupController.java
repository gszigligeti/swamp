package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampGroupDetails;
import hu.gszigligeti.swamp.exceptions.SwampGroupNotFoundException;
import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.services.SwampGroupService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Spring MVC controller for operations related to groups
 * @author Gergely Szigligeti
 */
@RestController
@RequestMapping("/api/groups")
public class SwampGroupController {
    @Autowired
    private SwampGroupService groupService;

    @Autowired
    private SwampUserService userService;

    // ~~~~ GROUP OPERATIONS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * This method is responsible for creating a new group in the system
     * @param groupOverview The details of the group to be created
     * @return HTTP 201 Created if the gruop has been created, otherwise HTTP 500 Internal Server Error
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createSwampGroup(@RequestBody @Valid SwampGroupDetails groupOverview) {
        final SwampGroup createdGroup = this.groupService.createSwampGroup(groupOverview);
        return new ResponseEntity(createdGroup == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.CREATED);
    }

    /**
     * This method is responsible for fetching every group from the database
     * @return HTTP 200 OK if there are any visible groups, otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findEverySwampGroup() {
        final Set<SwampGroup> groupList = this.groupService.findEverySwampGroup();
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        return new ResponseEntity<Set>(groupList.stream().map(g -> new SwampGroupDetails(g, authenticatedUser)).collect(Collectors.toSet()), HttpStatus.OK);
    }

    /**
     * This method is responsible for finding an existing group by it's identifier hash
     * @param groupId The identifier hash of the group
     * @return HTTP 200 and the group details if the group exists, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{groupId}", method = RequestMethod.GET)
    public ResponseEntity findSwampGroupById(@PathVariable String groupId) {
        final SwampGroup foundGroup = this.groupService.findSwampGroupById(groupId);
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampGroupDetails groupDetails = foundGroup == null ? null : new SwampGroupDetails(foundGroup, authenticatedUser);
        return new ResponseEntity<SwampGroupDetails>(groupDetails, groupDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating an existing group with new details
     * @param groupId The identifier hash of the group
     * @param groupOverview The object that contains the new group details
     * @return The updated group details
     */
    @RequestMapping(value = "/{groupId}", method = RequestMethod.PUT)
    public ResponseEntity updateSwampGroup(@PathVariable String groupId, @RequestBody SwampGroupDetails groupOverview) {
        final SwampGroup updatedGroup = this.groupService.updateSwampGroup(groupOverview);
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampGroupDetails groupDetails = updatedGroup == null ? null : new SwampGroupDetails(updatedGroup, authenticatedUser);
        return new ResponseEntity<SwampGroupDetails>(groupDetails, groupDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.NO_CONTENT);
    }

    /**
     * This method is responsible for removing an existing group by it's identifier hash
     * @param groupId The identifier hash of the group
     * @return HTTP 204 No content if the removal was successful, otherwise unauthorized exception is thrown
     */
    @RequestMapping(value = "/{groupId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteSwampGroupById(@PathVariable String groupId) throws SwampGroupNotFoundException {
        this.groupService.deleteSwampGroupById(groupId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    // ~~~~ EXCEPTION HANDLERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Exception handler method for handling if the group could not be found based on the provided details
     * @param exception The exception object injected by Spring
     * @return HTTP 404 Not found
     */
    @ExceptionHandler(SwampGroupNotFoundException.class)
    public ResponseEntity handleInvalidGroupDetails(SwampGroupNotFoundException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    /**
     * Exception handler method for handling if the user is unauthorized to execute a given operation
     * @param exception The exception object injected by Spring
     * @return HTTP 403 Forbidden
     */
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity handleUnauthorizedOperation(AccessDeniedException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.FORBIDDEN);
    }

    /**
     * Exception handler method for generic runtime exceptions
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleGenericRuntimeException(RuntimeException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
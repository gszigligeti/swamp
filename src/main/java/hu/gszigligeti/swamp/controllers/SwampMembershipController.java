package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampGroupMemberDetails;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.services.SwampMembershipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Spring MVC controller for operations related to group memberships
 * @author Gergely Szigligeti
 */
@RestController
@RequestMapping("/api/groups/{groupId}/members")
public class SwampMembershipController {
    @Autowired
    private SwampMembershipService membershipService;

    /**
     * This method is responsible for creating a new membership relation between the given group and user
     * @param groupId The group where the user is going to belong
     * @param userId The identifier if the existing user account
     * @param role The initial role of the new member
     * @return HTTP 201 Created and the member entity if the membership is created, otherwise HTTP 400 Bad request
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity addSwampGroupMembership(@PathVariable String groupId, @RequestParam String userId, @RequestParam Byte role) {
        SwampGroupMembership membership = this.membershipService.addSwampGroupMembership(groupId, userId, role);
        SwampGroupMemberDetails memberDetails = membership == null ? null : new SwampGroupMemberDetails(membership);
        return new ResponseEntity<SwampGroupMemberDetails>(memberDetails, memberDetails == null ? HttpStatus.BAD_REQUEST : HttpStatus.CREATED);
    }

    /**
     * This method is responsible for fetching every member of a given group
     * @param groupId The identifier of the group that we'd like to check
     * @return HTTP 200 OK and the list of memberships (if there are any), otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findMembersOfSwampGroup(@PathVariable String groupId) {
        Set<SwampGroupMembership> memberships = this.membershipService.findMembersOfSwampGroup(groupId);
        Set<SwampGroupMemberDetails> memberDetails = memberships == null ? Collections.emptySet() : memberships.stream().map(SwampGroupMemberDetails::new).collect(Collectors.toSet());
        return new ResponseEntity<Set>(memberDetails, memberDetails.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating an existing membership
     * @param groupId The group where the member belongs to
     * @param userId The identifier of the member which we want to update
     * @param role The new role of the member within the group
     * @return HTTP 200 OK and the new membership entity, otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateSwampGroupMembership(@PathVariable String groupId, @RequestParam String userId, @RequestParam Byte role) {
        SwampGroupMembership membership = this.membershipService.updateSwampGroupMembership(groupId, userId, role);
        SwampGroupMemberDetails memberDetails = membership == null ? null : new SwampGroupMemberDetails(membership);
        return new ResponseEntity<SwampGroupMemberDetails>(memberDetails, memberDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for deleting a member from a given group
     * @param groupId The group identifier
     * @param userId The identifier of the member which we want to remove
     * @return HTTP 204 No content if the member has been successfully removed
     */
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity deleteSwampGroupMembership(@PathVariable String groupId, @RequestParam String userId) {
        this.membershipService.deleteSwampGroupMembership(groupId, userId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Exception handler method for generic runtime exceptions
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleGenericRuntimeException(RuntimeException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
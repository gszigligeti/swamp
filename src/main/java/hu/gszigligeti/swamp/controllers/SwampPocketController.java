package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampPocketDetails;
import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.services.SwampPocketService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Spring MVC controller for operations related to pockets
 * @author Gergely Szigligeti
 */
@RestController
@RequestMapping("/api/pockets")
public class SwampPocketController {
    @Autowired
    private SwampPocketService pocketService;

    @Autowired
    private SwampUserService userService;

    /**
     * This method is responsible for creating a new pocket and assigning it to it's owner
     * @param ownerId The identifier of the owner (user or group)
     * @param pocketType The type of the pocket (user or group)
     * @param pocketDetails The details of the new pocket entity
     * @return HTTP 201 Created and the new pocket entity if it was successfully persisted, otherwise HTTP 400 Bad request
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createNewSwampPocket(@RequestParam String ownerId, @RequestParam String pocketType, @RequestBody SwampPocketDetails pocketDetails) {
        final SwampAbstractPocket createdPocket = this.pocketService.createNewSwampPocket(ownerId, pocketType, pocketDetails);
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampPocketDetails savedPocketDetails = createdPocket == null ? null : new SwampPocketDetails(createdPocket, authenticatedUser);
        return new ResponseEntity<SwampPocketDetails>(savedPocketDetails, createdPocket == null ? HttpStatus.BAD_REQUEST : HttpStatus.CREATED);
    }

    /**
     * This method is responsible for getting every pocket assigned to a given owner (if parameter specified)
     * @param ownerId The owner identifier if we want to filter results
     * @return HTTP 200 OK and the list of pockets if there are any, otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findSwampPockets(@RequestParam(required = false) String ownerId) {
        final Set<SwampAbstractPocket> pockets = ownerId != null ? this.pocketService.findPocketsOfOwner(ownerId) : this.pocketService.findEverySwampPocket();
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        return new ResponseEntity<Set>(pockets.stream().map(p -> new SwampPocketDetails(p, authenticatedUser)).collect(Collectors.toSet()), HttpStatus.OK);
    }

    /**
     * This method is responsible for fetching a pocket by it's identifier
     * @param pocketId The identifier of the pocket
     * @return HTTP 200 OK and the pocket instance if the pocket is accessible, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{pocketId}", method = RequestMethod.GET)
    public ResponseEntity findSwampPocketById(@PathVariable String pocketId) {
        final SwampAbstractPocket fetchedPocket = this.pocketService.findSwampPocketById(pocketId);
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampPocketDetails fetchedPocketDetails = fetchedPocket == null ? null : new SwampPocketDetails(fetchedPocket, authenticatedUser);
        return new ResponseEntity<SwampPocketDetails>(fetchedPocketDetails, fetchedPocket == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating a pocket with the provided new details
     * @param pocketId The identifier of the pocket which needs to be updated
     * @param pocketDetails The new details of the pocket
     * @return HTTP 200 OK and the new pocket details if the update was successful, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{pocketId}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateExistingSwampPocket(@PathVariable String pocketId, @RequestBody SwampPocketDetails pocketDetails) {
        final SwampAbstractPocket updatedPocket = this.pocketService.updateExistingSwampPocket(pocketDetails);
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampPocketDetails updatedPocketDetails = updatedPocket == null ? null : new SwampPocketDetails(updatedPocket, authenticatedUser);
        return new ResponseEntity<SwampPocketDetails>(updatedPocketDetails, updatedPocket == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for removing a given pocket
     * @param pocketId The identifier of the pocket which needs to be deleted
     * @return HTTP 204 No content if the removal was successful
     */
    @RequestMapping(value = "/{pocketId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteExistingSwampPocket(@PathVariable String pocketId) {
        this.pocketService.deleteExistingSwampPocket(pocketId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
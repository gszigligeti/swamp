package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampPocketEntryDetails;
import hu.gszigligeti.swamp.models.SwampPocketEntry;
import hu.gszigligeti.swamp.services.SwampPocketEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Spring MVC controller for operations related to pocket entries
 * @author Gergely Szigligeti
 */
@RestController
@RequestMapping("/api/entries")
public class SwampPocketEntryController {
    @Autowired
    private SwampPocketEntryService pocketEntryService;

    /**
     * This method is responsible for creating a new pocket entry in a given pocket
     * @param pocketEntryDetails The details of the new entry
     * @return HTTP 201 Created and the persisted entity if the operation was successful, otherwise HTTP 400 Bad request
     */
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createNewSwampPocketEntry(@RequestBody SwampPocketEntryDetails pocketEntryDetails) throws IOException {
        SwampPocketEntry pocketEntry = this.pocketEntryService.addPocketEntry(pocketEntryDetails);
        SwampPocketEntryDetails entryDetails = pocketEntry == null ? null : new SwampPocketEntryDetails(pocketEntry);
        return new ResponseEntity<SwampPocketEntryDetails>(entryDetails, entryDetails == null ? HttpStatus.BAD_REQUEST : HttpStatus.CREATED);
    }

    /**
     * This method is responsible for fetching every entry within a given pocket (if specified)
     * @param ownerId The identifier of the parent pocket
     * @return HTTP 200 OK and the list of entries if there are any, otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findEntriesOfOwner(@RequestParam(required = false) String ownerId) {
        Set<SwampPocketEntry> entryList = ownerId != null ? this.pocketEntryService.findEntriesOfOwner(ownerId) : this.pocketEntryService.findEveryPocketEntry();
        Set<SwampPocketEntryDetails> entryDetails = entryList == null ? Collections.emptySet() : entryList.stream().map(SwampPocketEntryDetails::new).collect(Collectors.toSet());
        return new ResponseEntity<Set>(entryDetails, HttpStatus.OK);
    }

    /**
     * This method is responsible for fetching a pocket entry by it's identifier
     * @param entryId The identifier of the pocket entry
     * @return HTTP 200 OK and the entry entity if it was found, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{entryId}", method = RequestMethod.GET)
    public ResponseEntity findPocketEntryById(@PathVariable String entryId) {
        SwampPocketEntry pocketEntry = this.pocketEntryService.findPocketEntryById(entryId);
        SwampPocketEntryDetails entryDetails = pocketEntry == null ? null : new SwampPocketEntryDetails(pocketEntry);
        return new ResponseEntity<SwampPocketEntryDetails>(entryDetails, entryDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating an existing pocket entry with new details
     * @param entryId The identifier of the entry which we want to update
     * @param pocketEntryDetails The new details of the pocket entry
     * @return HTTP 200 OK and the new entry details if the update was successful, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{entryId}", method = RequestMethod.PUT)
    public ResponseEntity updatePocketEntry(@PathVariable String entryId, @RequestBody SwampPocketEntryDetails pocketEntryDetails) {
        SwampPocketEntry pocketEntry = this.pocketEntryService.updatePocketEntry(pocketEntryDetails);
        SwampPocketEntryDetails entryDetails = pocketEntry == null ? null : new SwampPocketEntryDetails(pocketEntry);
        return new ResponseEntity<SwampPocketEntryDetails>(entryDetails, entryDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for removing a entry from a given pocket
     * @param entryId The identifier of the entry to be removed
     * @return HTTP 204 No content if the removal was successful
     */
    @RequestMapping(value = "/{entryId}", method = RequestMethod.DELETE)
    public ResponseEntity deletePocketEntry(@PathVariable String entryId) {
        this.pocketEntryService.deletePocketEntry(entryId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Exception handler method for IO runtime exceptions (e.g. GCM messaging failure)
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(IOException.class)
    public ResponseEntity handleIOException(IOException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
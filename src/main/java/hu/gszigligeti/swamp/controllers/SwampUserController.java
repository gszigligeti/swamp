package hu.gszigligeti.swamp.controllers;

import hu.gszigligeti.swamp.controllers.dto.SwampAccountDetails;
import hu.gszigligeti.swamp.controllers.dto.SwampAccountOverview;
import hu.gszigligeti.swamp.controllers.dto.SwampAccountUpdateRequest;
import hu.gszigligeti.swamp.models.SwampCloudToken;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.services.SwampCloudMessagingService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class SwampUserController {
    @Autowired
    private SwampUserService userService;

    @Autowired
    private SwampCloudMessagingService cloudMessagingService;

    /**
     * This method is responsible for getting every user in the system
     * @return HTTP 200 OK and the list of users (if not empty), otherwise HTTP 404 Not found
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity findEverySwampUser() {
        Set<SwampUser> users = this.userService.findEverySwampUser();
        Set<SwampAccountOverview> accountOverview = users == null ? Collections.emptySet() : users.stream().map(SwampAccountOverview::new).collect(Collectors.toSet());
        return new ResponseEntity<Set>(accountOverview, accountOverview.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for fetching a given user by it's identifier
     * @param userId The identifier of the user
     * @return HTTP 200 OK and the user entity if it can be found, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    public ResponseEntity findSwampUserById(@PathVariable String userId) {
        SwampUser fetchedUser = this.userService.findSwampUserById(userId);
        SwampAccountDetails accountDetails = fetchedUser == null ? null : new SwampAccountDetails(fetchedUser);
        return new ResponseEntity<SwampAccountDetails>(accountDetails, accountDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for updating a user account's details
     * @param userId The identifier of the account
     * @param updateRequest The incoming HTTP request body
     * @return HTTP 200 OK and the updated user entity if the update was successful, otherwise HTTP 404 Not found
     */
    @RequestMapping(value = "/{userId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updateSwampUserDetails(@PathVariable String userId, @RequestBody SwampAccountUpdateRequest updateRequest) {
        SwampUser updatedUser = this.userService.updateSwampUserAccount(userId, updateRequest);
        SwampAccountDetails updatedAccountDetails = updatedUser == null ? null : new SwampAccountDetails(updatedUser);
        return new ResponseEntity<SwampAccountDetails>(updatedAccountDetails, updatedAccountDetails == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }

    /**
     * This method is responsible for deleting a user account from the system
     * @param userId The identifier of the user
     * @param password The confirmation password
     * @return HTTP 204 No content if the user has been deleted
     */
    @RequestMapping(value = "/{userId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteSwampUserById(@PathVariable String userId, @RequestParam(required = false) String password) {
        this.userService.deleteSwampUserAccount(userId, password);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * This method is responsible for adding a new GCM registration token to a given user
     * @param userId The identifier of the user who owns the token
     * @param registrationId The device registration id received from Google
     * @return HTTP 200 if the registration was successful, otherwise HTTP 500 Internal Server Error
     */
    @RequestMapping(value = "/{userId}/cloud/tokens", method = RequestMethod.POST)
    public ResponseEntity registerDeviceToUser(@PathVariable String userId, @RequestParam(required = false) String registrationId) {
        SwampCloudToken cloudToken = this.cloudMessagingService.registerDeviceToSwampUser(userId, registrationId);
        return new ResponseEntity<SwampCloudToken>(cloudToken, cloudToken == null ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * This method is responsible for removing GCM token from a given user
     * @param userId The identifier of the user who owns the token
     * @param registrationId The registration id which needs to be deleted
     * @return HTTP 204 No content if the removal was successful
     */
    @RequestMapping(value = "/{userId}/cloud/tokens", method = RequestMethod.DELETE)
    public ResponseEntity unregisterDeviceFromUser(@PathVariable String userId, @RequestParam(required = false) String registrationId) {
        this.cloudMessagingService.unregisterDeviceFromSwampUser(userId, registrationId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    /**
     * Exception handler method for generic runtime exceptions
     * @param exception The exception object injected by Spring
     * @return HTTP 500 Internal Server Error
     */
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity handleGenericRuntimeException(RuntimeException exception) {
        return new ResponseEntity<Map>(Collections.singletonMap("errorMessage", exception.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
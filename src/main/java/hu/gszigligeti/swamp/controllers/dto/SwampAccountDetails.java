package hu.gszigligeti.swamp.controllers.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hu.gszigligeti.swamp.models.SwampUser;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Data transfer object for Swamp account details
 * @author Gergely Szigligeti
 */
public class SwampAccountDetails implements Serializable {
    private String userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String password;
    private Boolean isActivated;
    private Boolean isPublic;
    private Boolean isSuspended;
    private String profilePicture;
    private String language;
    private Date registrationDate = new Date();
    private Set<String> roles = new HashSet<String>();

    public SwampAccountDetails() {
        // Empty constructor
    }

    public SwampAccountDetails(SwampUser authenticatedUser) {
        this.userId = authenticatedUser.getId();
        this.firstName = authenticatedUser.getFirstName();
        this.lastName = authenticatedUser.getLastName();
        this.emailAddress = authenticatedUser.getEmailAddress();
        this.password = authenticatedUser.getPassword();
        this.isActivated = authenticatedUser.getActivated();
        this.isPublic = authenticatedUser.getPublic();
        this.isSuspended = authenticatedUser.getSuspended();
        this.profilePicture = authenticatedUser.getProfilePicture();
        this.language = authenticatedUser.getLanguage();
        this.registrationDate = authenticatedUser.getRegistrationDate();
        this.roles = authenticatedUser.getRoles();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Boolean getSuspended() {
        return isSuspended;
    }

    public void setSuspended(Boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }
}
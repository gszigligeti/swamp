package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampUser;

import java.io.Serializable;

/**
 * Data transfer object for short Swamp account overview
 * @author Gergely Szigligeti
 */
public class SwampAccountOverview implements Serializable {
    private String userId;
    private String firstName;
    private String lastName;
    private String profilePicture;

    public SwampAccountOverview() {
        // Empty constructor
    }

    public SwampAccountOverview(SwampUser authenticatedUser) {
        this.userId = authenticatedUser.getId();
        this.firstName = authenticatedUser.getFirstName();
        this.lastName = authenticatedUser.getLastName();
        this.profilePicture = authenticatedUser.getProfilePicture();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
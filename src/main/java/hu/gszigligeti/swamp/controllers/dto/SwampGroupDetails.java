package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.utilities.SwampStreamUtilities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Data transfer object for Swamp group with quick overview
 * @author Gergely Szigligeti
 */
public class SwampGroupDetails implements Serializable {
    private String groupId;
    private String groupName;
    private String description;
    private Boolean isPublic;
    private Set<String> members = new HashSet<>();
    private SwampGroupMemberDetails membership;
    private Date creationDate = new Date();

    public SwampGroupDetails() {
        // Empty constructor
    }

    public SwampGroupDetails(SwampGroup swampGroup, SwampUser currentUser) {
        String[] roleList = {"MEMBER", "EDITOR", "ADMIN", "OWNER"};

        this.groupId = swampGroup.getId();
        this.groupName = swampGroup.getGroupName();
        this.description = swampGroup.getDescription();
        this.isPublic = swampGroup.getPublic();
        this.members = swampGroup.getMembers().stream().map(m -> m.getMembershipKey().getUser().getId()).collect(Collectors.toSet());

        swampGroup.getMembers().stream().filter(SwampStreamUtilities.groupMembershipFilter(currentUser)).forEach(groupMembership -> {
            this.membership = new SwampGroupMemberDetails(groupMembership);
        });

        this.creationDate = swampGroup.getCreationDate();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Set<String> getMembers() {
        return members;
    }

    public void setMembers(Set<String> members) {
        this.members = members;
    }

    public SwampGroupMemberDetails getMembership() {
        return membership;
    }

    public void setMembership(SwampGroupMemberDetails membership) {
        this.membership = membership;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
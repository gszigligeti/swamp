package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampUser;

import java.io.Serializable;

/**
 * Data transfer object for Swamp group member details
 * @author Gergely Szigligeti
 */
public class SwampGroupMemberDetails implements Serializable {
    private String userId;
    private String firstName;
    private String lastName;
    private String profilePicture;
    private String memberRole;
    private Byte membershipValue;

    public SwampGroupMemberDetails() {
        // Empty constructor
    }

    public SwampGroupMemberDetails(SwampGroupMembership groupMembership) {
        SwampUser member = groupMembership.getMembershipKey().getUser();
        String[] roleList = {"MEMBER", "EDITOR", "ADMIN", "OWNER"};

        this.userId = member.getId();
        this.firstName = member.getFirstName();
        this.lastName = member.getLastName();
        this.profilePicture = member.getProfilePicture();
        this.memberRole = roleList[(int) Math.round(Math.log(groupMembership.getRole()) / Math.log(2.0d))];
        this.membershipValue = groupMembership.getRole();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getMemberRole() {
        return memberRole;
    }

    public void setMemberRole(String memberRole) {
        this.memberRole = memberRole;
    }

    public Byte getMembershipValue() {
        return membershipValue;
    }

    public void setMembershipValue(Byte membershipValue) {
        this.membershipValue = membershipValue;
    }
}
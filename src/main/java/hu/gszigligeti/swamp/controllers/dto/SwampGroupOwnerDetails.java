package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampUser;

import java.io.Serializable;

/**
 * Data transfer object for basic details about the group owner
 * @author Gergely Szigligeti
 */
public class SwampGroupOwnerDetails implements Serializable {
    private String userId;
    private String firstName;
    private String lastName;

    public SwampGroupOwnerDetails() {
        // Empty constructor
    }

    public SwampGroupOwnerDetails(SwampUser groupOwner) {
        this.userId = groupOwner.getId();
        this.firstName = groupOwner.getFirstName();
        this.lastName = groupOwner.getLastName();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
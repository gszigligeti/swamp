package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampGroupPocket;
import hu.gszigligeti.swamp.models.SwampUser;

import java.io.Serializable;
import java.util.Date;

/**
 * Data transfer object for Swamp pocket details
 * @author Gergely Szigligeti
 */
public class SwampPocketDetails implements Serializable {
    private String pocketId;
    private String pocketName;
    private String description;
    private Integer initialValue;
    private Integer balance;
    private String currency;
    private Integer numberOfEntries;
    private SwampGroupDetails ownerGroup;
    private Date creationDate = new Date();

    public SwampPocketDetails() {
        // Empty constructor
    }

    public SwampPocketDetails(SwampAbstractPocket abstractPocket, SwampUser currentUser) {
        this.pocketId = abstractPocket.getId();
        this.pocketName = abstractPocket.getPocketName();
        this.description = abstractPocket.getDescription();
        this.initialValue = abstractPocket.getInitialValue();
        this.balance = abstractPocket.getBalance();
        this.currency = abstractPocket.getCurrency();
        this.numberOfEntries = abstractPocket.getPocketEntries().size();

        if (abstractPocket instanceof SwampGroupPocket) {
            this.ownerGroup = new SwampGroupDetails(((SwampGroupPocket) abstractPocket).getPocketOwner(), currentUser);
        }

        this.creationDate = abstractPocket.getCreationDate();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getPocketId() {
        return pocketId;
    }

    public void setPocketId(String pocketId) {
        this.pocketId = pocketId;
    }

    public String getPocketName() {
        return pocketName;
    }

    public void setPocketName(String pocketName) {
        this.pocketName = pocketName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Integer initialValue) {
        this.initialValue = initialValue;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getNumberOfEntries() {
        return numberOfEntries;
    }

    public void setNumberOfEntries(Integer numberOfEntries) {
        this.numberOfEntries = numberOfEntries;
    }

    public SwampGroupDetails getOwnerGroup() {
        return ownerGroup;
    }

    public void setOwnerGroup(SwampGroupDetails ownerGroup) {
        this.ownerGroup = ownerGroup;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
package hu.gszigligeti.swamp.controllers.dto;

import hu.gszigligeti.swamp.models.SwampPocketEntry;

import java.io.Serializable;
import java.util.Date;

/**
 * Data transfer object for Swamp pocket entry objects
 * @author Gergely Szigligeti
 */
public class SwampPocketEntryDetails implements Serializable {
    private String entryId;
    private String entryName;
    private String description;
    private Integer amount;
    private String parentPocket;
    private Date entryDate = new Date();

    public SwampPocketEntryDetails() {
        // Empty constructor
    }

    public SwampPocketEntryDetails(SwampPocketEntry pocketEntry) {
        this.entryId = pocketEntry.getId();
        this.entryName = pocketEntry.getEntryName();
        this.description = pocketEntry.getDescription();
        this.amount = pocketEntry.getAmount();
        this.parentPocket = pocketEntry.getParentPocket().getId();
        this.entryDate = pocketEntry.getEntryDate();
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getParentPocket() {
        return parentPocket;
    }

    public void setParentPocket(String parentPocket) {
        this.parentPocket = parentPocket;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
}
package hu.gszigligeti.swamp.exceptions;

/**
 * Exception class to be thrown if the system could not find the group with the given details
 * @author Gergely Szigligeti
 */
public class SwampGroupNotFoundException extends RuntimeException {
    public SwampGroupNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public SwampGroupNotFoundException(String message) {
        super(message);
    }
}
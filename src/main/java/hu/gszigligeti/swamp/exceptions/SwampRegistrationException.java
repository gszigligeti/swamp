package hu.gszigligeti.swamp.exceptions;

/**
 * Exception class for preventing users from registering with invalid user details
 * @author Gergely Szigligeti
 */
public class SwampRegistrationException extends RuntimeException {
    public SwampRegistrationException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public SwampRegistrationException(String message) {
        super(message);
    }
}
package hu.gszigligeti.swamp.exceptions;

/**
 * Exception class to be thrown if the system could not find the user with the given details
 * @author Gergely Szigligeti
 */
public class SwampUserNotFoundException extends RuntimeException {
    public SwampUserNotFoundException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public SwampUserNotFoundException(String message) {
        super(message);
    }
}
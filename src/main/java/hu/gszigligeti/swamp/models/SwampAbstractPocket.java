package hu.gszigligeti.swamp.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Abstract base class for modeling generic pocket entities
 * @author Gergely Szigligeti
 */
@Entity
@Inheritance
@Table(name = "POCKETS")
@DiscriminatorColumn(name = "POCKET_TYPE")
public abstract class SwampAbstractPocket implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "POCKET_NAME", length = 50, nullable = false)
    private String pocketName;

    @Column(name = "DESCRIPTION", length = 100, nullable = true)
    private String description;

    @NotNull
    @Column(name = "INITIAL_VALUE", nullable = false)
    private Integer initialValue = 0;

    @NotNull
    @Column(name = "BALANCE", nullable = false)
    private Integer balance = 0;

    @Column(name = "POCKET_TYPE", length = 10, insertable = false, updatable = false)
    private String pocketType;

    @NotNull
    @Column(name = "CURRENCY", length = 3, nullable = false)
    private String currency;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATION_DATE", updatable = false)
    private Date creationDate = new Date();

    @OneToMany(mappedBy = "parentPocket", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SwampPocketEntry> pocketEntries = new HashSet<SwampPocketEntry>();

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPocketName() {
        return pocketName;
    }

    public void setPocketName(String pocketName) {
        this.pocketName = pocketName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Integer initialValue) {
        this.initialValue = initialValue;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public String getPocketType() {
        return pocketType;
    }

    public void setPocketType(String pocketType) {
        this.pocketType = pocketType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Set<SwampPocketEntry> getPocketEntries() {
        return pocketEntries;
    }

    public void setPocketEntries(Set<SwampPocketEntry> pocketEntries) {
        this.pocketEntries = pocketEntries;
    }
}
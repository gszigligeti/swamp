package hu.gszigligeti.swamp.models;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Entity model representation of a GCM token
 * @author Gergely Szigligeti
 */
@Entity
@Table(name = "CLOUD_TOKENS")
@AssociationOverrides({
    @AssociationOverride(name = "cloudTokenKey.user", joinColumns = @JoinColumn(name = "USER_ID")),
    @AssociationOverride(name = "cloudTokenKey.deviceToken", joinColumns = @JoinColumn(name = "DEVICE_TOKEN"))
})
public class SwampCloudToken implements Serializable {
    @EmbeddedId
    private CloudTokenKey cloudTokenKey = new CloudTokenKey();

    public SwampCloudToken() {
        // Empty constructor for Hibernate
    }

    public SwampCloudToken(CloudTokenKey cloudTokenKey) {
        this.cloudTokenKey = cloudTokenKey;
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public CloudTokenKey getCloudTokenKey() {
        return cloudTokenKey;
    }

    public void setCloudTokenKey(CloudTokenKey cloudTokenKey) {
        this.cloudTokenKey = cloudTokenKey;
    }

    @Embeddable
    public static class CloudTokenKey implements Serializable {
        @ManyToOne(cascade = CascadeType.ALL)
        private SwampUser user;

        @NotNull
        @Column(name = "DEVICE_TOKEN", length = 4096, nullable = false)
        private String deviceToken;

        public CloudTokenKey() {
            // Empty constructor for Hibernate
        }

        public CloudTokenKey(SwampUser user, String deviceToken) {
            this.user = user;
            this.deviceToken = deviceToken;
        }

        // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        public SwampUser getUser() {
            return user;
        }

        public void setUser(SwampUser user) {
            this.user = user;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }
    }
}
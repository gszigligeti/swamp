package hu.gszigligeti.swamp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hu.gszigligeti.swamp.controllers.dto.SwampGroupDetails;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity model representation of a Swamp group
 * @author Gergely Szigligeti
 */
@Entity
@Table(name = "GROUPS")
public class SwampGroup implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "GROUP_NAME", length = 50, nullable = false)
    private String groupName;

    @Column(name = "DESCRIPTION", length = 100, nullable = true)
    private String description;

    @NotNull
    @Column(name = "IS_PUBLIC", nullable = false)
    private Boolean isPublic = false;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "CREATION_DATE", updatable = false)
    private Date creationDate = new Date();

    @JsonIgnore
    @OneToMany(mappedBy = "membershipKey.group", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<SwampGroupMembership> members = new HashSet<SwampGroupMembership>();

    @JsonIgnore
    @OneToMany(mappedBy = "pocketOwner", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SwampGroupPocket> groupPockets = new HashSet<SwampGroupPocket>();

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Set<SwampGroupMembership> getMembers() {
        return members;
    }

    public void setMembers(Set<SwampGroupMembership> members) {
        this.members = members;
    }

    public Set<SwampGroupPocket> getGroupPockets() {
        return groupPockets;
    }

    public void setGroupPockets(Set<SwampGroupPocket> groupPockets) {
        this.groupPockets = groupPockets;
    }

    /**
     * Static helper method to easily convert data transfer objects into entities
     * @param groupOverview The reference transfer object
     * @return The converted entity
     */
    public static SwampGroup fromDTO(SwampGroupDetails groupOverview) {
        SwampGroup convertedGroup = new SwampGroup();

        convertedGroup.setGroupName(groupOverview.getGroupName());
        convertedGroup.setDescription(groupOverview.getDescription());
        convertedGroup.setPublic(groupOverview.getPublic());

        return convertedGroup;
    }
}
package hu.gszigligeti.swamp.models;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Entity model representation of relationship between users and groups
 * @author Gergely Szigligeti
 */
@Entity
@Table(name = "GROUP_MEMBERS")
@AssociationOverrides({
    @AssociationOverride(name = "membershipKey.group", joinColumns = @JoinColumn(name = "GROUP_ID")),
    @AssociationOverride(name = "membershipKey.user", joinColumns = @JoinColumn(name = "USER_ID"))
})
public class SwampGroupMembership implements Serializable {
    @EmbeddedId
    private MembershipKey membershipKey;

    @NotNull
    @Column(name = "ROLE", length = 20, nullable = false)
    private Byte role;

    public SwampGroupMembership() {
        // Empty constructor for Hibernate
    }

    public SwampGroupMembership(MembershipKey membershipKey, Byte role) {
        this.membershipKey = membershipKey;
        this.role = role;
    }

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public MembershipKey getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(MembershipKey membershipKey) {
        this.membershipKey = membershipKey;
    }

    public Byte getRole() {
        return role;
    }

    public void setRole(Byte role) {
        this.role = role;
    }

    @Embeddable
    public static class MembershipKey implements Serializable {
        @ManyToOne(cascade = CascadeType.ALL)
        private SwampGroup group;

        @ManyToOne(cascade = CascadeType.ALL)
        private SwampUser user;

        public MembershipKey() {
            // Empty constructor
        }

        public MembershipKey(SwampGroup group, SwampUser user) {
            this.group = group;
            this.user = user;
        }

        public SwampGroup getGroup() {
            return group;
        }

        public void setGroup(SwampGroup group) {
            this.group = group;
        }

        public SwampUser getUser() {
            return user;
        }

        public void setUser(SwampUser user) {
            this.user = user;
        }
    }
}
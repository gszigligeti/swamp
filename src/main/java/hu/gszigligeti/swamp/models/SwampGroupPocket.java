package hu.gszigligeti.swamp.models;

import hu.gszigligeti.swamp.constants.SwampPocketTypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Entity model for pocket with group owner, extending the abstract base class
 * @author Gergely Szigligeti
 */
@Entity
@DiscriminatorValue(SwampPocketTypes.GROUP)
public class SwampGroupPocket extends SwampAbstractPocket implements Serializable {
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_GROUP_ID", referencedColumnName = "ID")
    private SwampGroup pocketOwner;

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public SwampGroup getPocketOwner() {
        return pocketOwner;
    }

    public void setPocketOwner(SwampGroup pocketOwner) {
        this.pocketOwner = pocketOwner;
    }
}
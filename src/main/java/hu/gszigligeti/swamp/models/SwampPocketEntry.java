package hu.gszigligeti.swamp.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * Entity model representation of a Swamp pocket entry
 * @author Gergely Szigligeti
 */
@Entity
@Table(name = "POCKET_ENTRIES")
public class SwampPocketEntry implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @Column(name = "ENTRY_NAME", length = 50, nullable = true)
    private String entryName;

    @Column(name = "ENTRY_DESCRIPTION", length = 100, nullable = true)
    private String description;

    @NotNull
    @Column(name = "AMOUNT", nullable = false)
    private Integer amount;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "PARENT_POCKET_ID", referencedColumnName = "ID")
    private SwampAbstractPocket parentPocket;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ENTRY_DATE", updatable = false)
    private Date entryDate = new Date();

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public SwampAbstractPocket getParentPocket() {
        return parentPocket;
    }

    public void setParentPocket(SwampAbstractPocket parentPocket) {
        this.parentPocket = parentPocket;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
}
package hu.gszigligeti.swamp.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hu.gszigligeti.swamp.controllers.dto.SwampRegistrationRequest;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity model representation of a Swamp user
 * @author Gergely Szigligeti
 */
@Entity
@Table(name = "USERS")
public class SwampUser implements Serializable {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    private String id;

    @NotNull
    @Column(name = "FIRST_NAME", length = 50, nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "LAST_NAME", length = 50, nullable = false)
    private String lastName;

    @Email
    @NotNull
    @Column(name = "EMAIL_ADDRESS", length = 60, nullable = false)
    private String emailAddress;

    @NotNull
    @JsonIgnore
    @Column(name = "PASSWORD", length = 72, nullable = false)
    private String password;

    @NotNull
    @JsonIgnore
    @Column(name = "IS_ACTIVATED")
    private Boolean isActivated = false;

    @NotNull
    @Column(name = "IS_PUBLIC")
    private Boolean isPublic = true;

    @NotNull
    @JsonIgnore
    @Column(name = "IS_SUSPENDED")
    private Boolean isSuspended = false;

    @NotNull
    @Column(name = "PROFILE_PICTURE", length = 50, nullable = false)
    private String profilePicture;

    @NotNull
    @Column(name = "LANGUAGE", length = 2)
    @Pattern(regexp = "^(en|hu)$")
    private String language;

    @JsonIgnore
    @Column(name = "ACTIVATION_CODE", length = 72)
    private String activationCode;

    @JsonIgnore
    @Column(name = "PASSWORD_RESET_TOKEN", length = 72)
    private String passwordResetToken;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "PASSWORD_RESET_TOKEN_EXP")
    private Date passwordResetTokenExpiration;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "REGISTRATION_DATE", updatable = false)
    private Date registrationDate = new Date();

    @JsonIgnore
    @Column(name = "ROLE", nullable = false)
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "USERS_ROLES", joinColumns = @JoinColumn(name = "USER_ID"))
    private Set<String> roles = new HashSet<String>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "membershipKey.user", cascade = CascadeType.ALL)
    private Set<SwampGroupMembership> memberships = new HashSet<SwampGroupMembership>();

    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pocketOwner", cascade = CascadeType.ALL)
    private Set<SwampUserPocket> userPockets = new HashSet<SwampUserPocket>();

    @JsonIgnore
    @OneToMany(mappedBy = "cloudTokenKey.user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<SwampCloudToken> deviceTokens = new HashSet<SwampCloudToken>();

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getActivated() {
        return isActivated;
    }

    public void setActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean isPublic) {
        this.isPublic = isPublic;
    }

    public Boolean getSuspended() {
        return isSuspended;
    }

    public void setSuspended(Boolean isSuspended) {
        this.isSuspended = isSuspended;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public Date getPasswordResetTokenExpiration() {
        return passwordResetTokenExpiration;
    }

    public void setPasswordResetTokenExpiration(Date passwordResetTokenExpiration) {
        this.passwordResetTokenExpiration = passwordResetTokenExpiration;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Set<SwampGroupMembership> getMemberships() {
        return memberships;
    }

    public void setMemberships(Set<SwampGroupMembership> memberships) {
        this.memberships = memberships;
    }

    public Set<SwampUserPocket> getUserPockets() {
        return userPockets;
    }

    public void setUserPockets(Set<SwampUserPocket> userPockets) {
        this.userPockets = userPockets;
    }

    public Set<SwampCloudToken> getDeviceTokens() {
        return deviceTokens;
    }

    public void setDeviceTokens(Set<SwampCloudToken> deviceTokens) {
        this.deviceTokens = deviceTokens;
    }

    /**
     * Static helper method to easily convert data transfer objects into entities
     * @param request The reference transfer object
     * @return The converted entity
     */
    public static SwampUser fromDTO(SwampRegistrationRequest request) {
        SwampUser convertedUser = new SwampUser();

        convertedUser.setFirstName(request.getFirstName());
        convertedUser.setLastName(request.getLastName());
        convertedUser.setEmailAddress(request.getEmailAddress().toLowerCase());
        convertedUser.setPassword(request.getPassword());
        convertedUser.setLanguage(request.getLanguage());

        return convertedUser;
    }
}
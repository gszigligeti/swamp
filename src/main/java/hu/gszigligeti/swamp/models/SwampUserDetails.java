package hu.gszigligeti.swamp.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;

/**
 * Special model class for storing user details in security context
 * @author Gergely Szigligeti
 */
public class SwampUserDetails implements UserDetails {
    private SwampUser userPrincipal;
    private Boolean accountExpired;
    private Boolean credentialsExpired;
    private Set<GrantedAuthority> authorities;

    public SwampUserDetails(SwampUser userPrincipal, Set<GrantedAuthority> authorities) {
        this(userPrincipal, false, false, authorities);
    }

    public SwampUserDetails(SwampUser userPrincipal, Boolean accountExpired, Boolean credentialsExpired, Set<GrantedAuthority> authorities) {
        this.userPrincipal = userPrincipal;
        this.accountExpired = accountExpired;
        this.credentialsExpired = credentialsExpired;
        this.authorities = authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.userPrincipal.getPassword();
    }

    @Override
    public String getUsername() {
        return this.userPrincipal.getEmailAddress();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !this.accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !this.userPrincipal.getSuspended();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !this.credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.userPrincipal.getActivated();
    }

    public SwampUser getUserPrincipal() {
        return userPrincipal;
    }

    public void setUserPrincipal(SwampUser userPrincipal) {
        this.userPrincipal = userPrincipal;
    }

    public void setAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
}
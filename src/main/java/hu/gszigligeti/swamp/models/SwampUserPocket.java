package hu.gszigligeti.swamp.models;

import hu.gszigligeti.swamp.constants.SwampPocketTypes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Entity model for pocket with user owner, extending the abstract base class
 * @author Gergely Szigligeti
 */
@Entity
@DiscriminatorValue(SwampPocketTypes.USER)
public class SwampUserPocket extends SwampAbstractPocket implements Serializable {
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_USER_ID", referencedColumnName = "ID")
    private SwampUser pocketOwner;

    // ~~~~ GETTERS/SETTERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public SwampUser getPocketOwner() {
        return pocketOwner;
    }

    public void setPocketOwner(SwampUser pocketOwner) {
        this.pocketOwner = pocketOwner;
    }
}
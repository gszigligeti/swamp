package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Generic interface for the different specific pocket types
 * @author Gergely Szigligeti
 */
@NoRepositoryBean
public interface GenericSwampPocketRepository<T extends SwampAbstractPocket> extends JpaRepository<T, String> {

}
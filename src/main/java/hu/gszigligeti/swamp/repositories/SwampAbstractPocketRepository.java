package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository extension of the generic interface specified for generic pockets
 * @author Gergely Szigligeti
 */
@Transactional
public interface SwampAbstractPocketRepository extends GenericSwampPocketRepository<SwampAbstractPocket> {

}
package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampCloudToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for providing basic database operations on cloud token entities
 * @author Gergely Szigligeti
 */
@Repository
public interface SwampCloudTokenRepository extends JpaRepository<SwampCloudToken, SwampCloudToken.CloudTokenKey> {
    @Query("SELECT t FROM SwampCloudToken t WHERE t.cloudTokenKey.user.id = ?1 AND t.cloudTokenKey.deviceToken = ?2")
    public SwampCloudToken findByCloudTokenKey(String ownerUserId, String deviceToken);
}
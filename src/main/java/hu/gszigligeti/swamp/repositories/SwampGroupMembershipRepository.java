package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampGroupMembership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for providing basic database operations on group membership entities
 * @author Gergely Szigligeti
 */
@Repository
public interface SwampGroupMembershipRepository extends JpaRepository<SwampGroupMembership, SwampGroupMembership.MembershipKey> {
    @Query("SELECT m FROM SwampGroupMembership m WHERE m.membershipKey.group.id = ?1 AND m.membershipKey.user.id = ?2")
    public SwampGroupMembership findByMembershipKey(String groupId, String userId);
}
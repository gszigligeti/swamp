package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampGroupPocket;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository extension of the generic interface specified for pockets with group owner
 * @author Gergely Szigligeti
 */
@Transactional
public interface SwampGroupPocketRepository extends GenericSwampPocketRepository<SwampGroupPocket> {

}
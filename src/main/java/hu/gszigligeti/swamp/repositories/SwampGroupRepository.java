package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for providing basic database operations on group entities
 * @author Gergely Szigligeti
 */
@Repository
public interface SwampGroupRepository extends JpaRepository<SwampGroup, String> {

}
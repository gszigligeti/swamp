package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampPocketEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Repository interface for providing basic database operations on pocket entry entities
 * @author Gergely Szigligeti
 */
@Repository
public interface SwampPocketEntryRepository extends JpaRepository<SwampPocketEntry, String> {
    public Set<SwampPocketEntry> findAllByParentPocket(SwampAbstractPocket parentPocket);
}
package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampUserPocket;
import org.springframework.transaction.annotation.Transactional;

/**
 * Repository extension of the generic interface specified for pockets with group owner
 * @author Gergely Szigligeti
 */
@Transactional
public interface SwampUserPocketRepository extends GenericSwampPocketRepository<SwampUserPocket> {

}
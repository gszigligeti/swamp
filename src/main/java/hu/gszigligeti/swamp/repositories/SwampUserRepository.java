package hu.gszigligeti.swamp.repositories;

import hu.gszigligeti.swamp.models.SwampUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for providing basic database operations on user entities
 * @author Gergely Szigligeti
 */
@Repository
public interface SwampUserRepository extends JpaRepository<SwampUser, String> {
    /**
     * Spring Data query for a user filtered by the email address
     * @param emailAddress The email address which we want to search for
     * @return The user with the given email address (null if the user cannot be found)
     */
    public SwampUser findOneByEmailAddress(String emailAddress);

    /**
     * Spring Data query for a user filtered by the activation code
     * @param activationCode The activation code which we want to search for
     * @return The user with the given activation code (null if the user cannot be found)
     */
    public SwampUser findOneByActivationCode(String activationCode);

    /**
     * Spring Data query for a user filtered by the password reset hash
     * @param passwordResetToken The password reset hash which we want to search for
     * @return The user with the given password reset hash (null if the user cannot be found)
     */
    public SwampUser findOneByPasswordResetToken(String passwordResetToken);
}
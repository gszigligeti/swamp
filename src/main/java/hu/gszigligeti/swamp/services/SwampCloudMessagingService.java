package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.models.SwampCloudToken;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Service interface for GCM notifications and device registration
 * @author Gergely Szigligeti
 */
public interface SwampCloudMessagingService {
    /**
     * Method for registering a given device to a user
     * @param userId The identifier of the user who owns the device
     * @param registrationToken The registration token received from Google
     * @return The saved token entity
     */
    public SwampCloudToken registerDeviceToSwampUser(String userId, String registrationToken);

    /**
     * Method for sending push notifications to the list of devices with the given message
     * @param registrationIds The list of recipient device tokens
     * @param payload The message of the notification
     * @return True if the number of delivered messages equals the number of registration tokens
     * @throws IOException Thrown if there's a connection problem with GCM
     */
    public Boolean sendNotificationToDevices(List<String> registrationIds, Map<String, String> payload) throws IOException;

    /**
     * Method for removing a given device from a user
     * @param userId The identifier of the user who owns the device
     * @param registrationToken The registration token of the device
     */
    public void unregisterDeviceFromSwampUser(String userId, String registrationToken);
}
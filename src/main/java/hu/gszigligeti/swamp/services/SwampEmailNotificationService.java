package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.models.SwampUser;

/**
 * Service interface for email notification services (activation, password reset, etc.)
 * @author Gergely Szigligeti
 */
public interface SwampEmailNotificationService {
    /**
     * Method for sending activation email to freshly registered users
     * @param swampUser The model which the template will be generated from
     * @param host The base url of the website
     */
    public void sendAccountActivationEmail(final SwampUser swampUser, final String host);

    /**
     * Method for sending password reset email if the user does not remember the password
     * @param swampUser The model which the template will be generated from
     * @param host The base url of the website
     */
    public void sendAccountPasswordResetEmail(final SwampUser swampUser, final String host);
}
package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.controllers.dto.SwampGroupDetails;
import hu.gszigligeti.swamp.models.SwampGroup;

import java.util.Set;

/**
 * Interface for services with user group-related operations
 * @author Gergely Szigligeti
 */
public interface SwampGroupService {
    /**
     * Method responsible for creating a new group and setting the current user as the owner
     * @param groupOverview The incoming group creation request from the controller
     * @return If the operation was successful then the group's entity, otherwise null
     */
    public SwampGroup createSwampGroup(SwampGroupDetails groupOverview);

    /**
     * Method responsible for fetching groups from database while taking into account the user's role
     * @return The list of groups which are visible to the current user
     */
    public Set<SwampGroup> findEverySwampGroup();

    /**
     * Method responsible for finding an existing group by its identifier hash
     * @param groupId The identifier hash of the group
     * @return If the group exists and the user has sufficient privileges, then the group's entity
     */
    public SwampGroup findSwampGroupById(String groupId);

    /**
     * Method responsible for updating an existing group with new details
     * @param groupId The identifier hash of the group
     * @param groupOverview The object that contains the new group details
     * @return The updated group entity object
     */
    public SwampGroup updateSwampGroup(SwampGroupDetails groupOverview);

    /**
     * Method responsible for removing an existing group by its identifier hash
     * @param groupId The identifier hash of the group
     */
    public void deleteSwampGroupById(String groupId);
}
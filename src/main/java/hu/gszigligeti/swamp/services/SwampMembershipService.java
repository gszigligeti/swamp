package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampUser;

import java.util.Set;

/**
 * Interface for services with group membership-related operations
 * @author Gergely Szigligeti
 */
public interface SwampMembershipService {
    /**
     * Method responsible for creating a new membership relation between the given group and user
     * @param groupId The identifier of the group where the user is going to belong
     * @param userId The identifier of the existing user account
     * @param role The initial role of the new member
     * @return The member entity if the membership has been created
     */
    public SwampGroupMembership addSwampGroupMembership(String groupId, String userId, byte role);

    /**
     * Method responsible for creating a new superuser in the given group
     * @param group The group where the user is going to be a superuser
     * @param member The entity of the user who is going to be superuser
     * @return The owner entity if the membership has been created
     */
    public SwampGroupMembership addSwampGroupSuperuser(SwampGroup group, SwampUser member);

    /**
     * Method responsible for fetching every member of a given group
     * @param groupId The identifier of the group that we'd like to check
     * @return The list of memberships within the group (if there are any)
     */
    public Set<SwampGroupMembership> findMembersOfSwampGroup(String groupId);

    /**
     * Method responsible for finding a membership entity by the group and member identifier
     * @param groupId The group which the user belongs to
     * @param userId The identifier of the member
     * @return The membership entity, if it was found
     */
    public SwampGroupMembership findMemberById(String groupId, String userId);

    /**
     * Method responsible for updating an existing membership
     * @param groupId The group where the member belongs to
     * @param userId The identifier of the member which we want to update
     * @param role The new role of the member within the group
     * @return The updated membership entity
     */
    public SwampGroupMembership updateSwampGroupMembership(String groupId, String userId, byte role);

    /**
     * Method responsible for deleting a member from a given group
     * @param groupId The group identifier
     * @param userId The identifier of the member which we want to remove
     */
    public void deleteSwampGroupMembership(String groupId, String userId);
}
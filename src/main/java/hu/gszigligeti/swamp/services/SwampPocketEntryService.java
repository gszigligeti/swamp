package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.controllers.dto.SwampPocketEntryDetails;
import hu.gszigligeti.swamp.models.SwampPocketEntry;

import java.io.IOException;
import java.util.Set;

/**
 * Interface for services with pocket entry-related operations
 * @author Gergely Szigligeti
 */
public interface SwampPocketEntryService {
    /**
     * Method responsible for creating a new pocket entry in a given pocket
     * @param entryDetails The details of the new entry
     * @return The persisted entity if the operation was successful
     */
    public SwampPocketEntry addPocketEntry(SwampPocketEntryDetails entryDetails) throws IOException;

    /**
     * Method responsible for fetching every entry within a given pocket
     * @param pocketEntryOwnerId The identifier of the parent pocket
     * @return The list of entries in the specified pocket
     */
    public Set<SwampPocketEntry> findEntriesOfOwner(String pocketEntryOwnerId);

    /**
     * Method responsible for fetching every pocket entry entity
     * @return The list of pocket entry entities
     */
    public Set<SwampPocketEntry> findEveryPocketEntry();

    /**
     * Method responsible for fetching a pocket entry by it's identifier
     * @param pocketEntryId The identifier of the pocket entry
     * @return The entry entity if it was found
     */
    public SwampPocketEntry findPocketEntryById(String pocketEntryId);

    /**
     * Method responsible for updating an existing pocket entry with new details
     * @param entryDetails The new details of the pocket entry
     * @return The new entry details if the update was successful
     */
    public SwampPocketEntry updatePocketEntry(SwampPocketEntryDetails entryDetails);

    /**
     * Method responsible for removing a entry from a given pocket
     * @param pocketEntryId The identifier of the entry to be removed
     */
    public void deletePocketEntry(String pocketEntryId);
}
package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.controllers.dto.SwampPocketDetails;
import hu.gszigligeti.swamp.models.SwampAbstractPocket;

import java.util.Set;

/**
 * Interface for services with pocket-related operations
 * @author Gergely Szigligeti
 */
public interface SwampPocketService {
    /**
     * Method responsible for creating a new pocket and assigning it to it's owner
     * @param ownerId The identifier of the owner (user or group)
     * @param pocketType The type of the pocket (user or group)
     * @param pocketDetails The details of the new pocket entity
     * @return The saved pocket entity object
     */
    public SwampAbstractPocket createNewSwampPocket(String ownerId, String pocketType, SwampPocketDetails pocketDetails);

    /**
     * Method responsible for getting every pocket
     * @return The list of pocket entries
     */
    public Set<SwampAbstractPocket> findEverySwampPocket();

    /**
     * Method responsible for getting every pocket from a given owner
     * @param pocketOwnerId The owner identifier if we want to filter results
     * @return The list of pockets which belong to the specified owner
     */
    public Set<SwampAbstractPocket> findPocketsOfOwner(String pocketOwnerId);

    /**
     * Method responsible for for fetching a pocket by it's identifier
     * @param pocketId The identifier of the pocket
     * @return The returned pocket entity object
     */
    public SwampAbstractPocket findSwampPocketById(String pocketId);

    /**
     * Method responsible for updating a pocket with the provided new details
     * @param pocketDetails The new details of the pocket
     * @return The updated pocket entity object
     */
    public SwampAbstractPocket updateExistingSwampPocket(SwampPocketDetails pocketDetails);

    /**
     * Method responsible for removing a given pocket
     * @param pocketId The identifier of the pocket which needs to be deleted
     */
    public void deleteExistingSwampPocket(String pocketId);

    /**
     * Method responsible for adding a given amount to a pocket balance
     * @param pocket The pocket which needs to be modified
     * @param amount The amount that needs to be added
     */
    public SwampAbstractPocket addMoneyToPocket(SwampAbstractPocket pocket, Integer amount);

    /**
     * Method responsible for withdrawing money from a pocket balance
     * @param pocket The pocket which needs to be modified
     * @param amount The amount that needs to be subtracted
     */
    public SwampAbstractPocket withdrawMoneyFromPocket(SwampAbstractPocket pocket, Integer amount);
}
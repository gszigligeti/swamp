package hu.gszigligeti.swamp.services;

import hu.gszigligeti.swamp.controllers.dto.SwampAccountUpdateRequest;
import hu.gszigligeti.swamp.controllers.dto.SwampRegistrationRequest;
import hu.gszigligeti.swamp.exceptions.SwampRegistrationException;
import hu.gszigligeti.swamp.exceptions.SwampUserNotFoundException;
import hu.gszigligeti.swamp.models.SwampUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

/**
 * Interface for services with user account-related operations
 * @author Gergely Szigligeti
 */
public interface SwampUserService {
    /**
     * Method responsible for create a new Swamp user account and send the activation email to the given e-mail address
     * @param registrationRequest The incoming registration request from the controller
     * @param servletRequest The incoming HTTP request from the controller (needed to assemble activation link)
     * @return If the registration was successful then the user's entity, otherwise null
     * @throws SwampRegistrationException Thrown if the e-mail address exists or the password is not strong enough
     */
    public SwampUser registerNewUser(SwampRegistrationRequest registrationRequest, HttpServletRequest servletRequest) throws SwampRegistrationException;

    /**
     * Method responsible for setting the IS_ACTIVATED flag to true on a given users account
     * @param activationCode The activation code that has been sent to the user's e-mail address
     * @return If the activation was successful then the user's updated entity, otherwise null
     */
    public SwampUser activateUserAccount(String activationCode) throws SwampUserNotFoundException;

    /**
     * Method responsible for sending the user a password reset email
     * @param emailAddress The email address of the user
     * @param servletRequest The incoming HTTP request from the controller (needed to assemble password reset link)
     * @return If the hash generation was successful and the user exists, then the updated user entity
     */
    public SwampUser sendPasswordResetEmail(String emailAddress, HttpServletRequest servletRequest) throws SwampUserNotFoundException;

    /**
     * Method responsible for updating a user's password in case he/she does not remember it
     * @param passwordResetToken The secret token that has been sent to the user's e-mail address
     * @return If the password reset was successful then the user's updated entity, otherwise null
     */
    public SwampUser resetForgottenPassword(String passwordResetToken, String updatedPassword) throws SwampUserNotFoundException;

    /**
     * Method responsible for retrieving the currently authenticated user from the security context
     * @return The authenticated user's entity
     */
    public SwampUser getCurrentlyAuthenticatedUser();

    /**
     * Method responsible for updating an account with new details
     * @param userId The identifier of the user
     * @param updateRequest The incoming HTTP request body
     * @return The updated user entity
     */
    public SwampUser updateSwampUserAccount(String userId, SwampAccountUpdateRequest updateRequest);

    /**
     * Method responsible for deleting an account
     * @param userId The identifier of the user
     */
    public void deleteSwampUserAccount(String userId, String password);

    /**
     * Method responsible for retrieving a user by it's identifier hash
     * @param userId The identifier of the user
     * @return The user entity
     */
    public SwampUser findSwampUserById(String userId);

    /**
     * Method responsible for fetching every accessible user profile
     * @return The list of accessible profile details
     */
    public Set<SwampUser> findEverySwampUser();
}
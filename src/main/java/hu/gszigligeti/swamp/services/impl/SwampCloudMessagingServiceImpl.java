package hu.gszigligeti.swamp.services.impl;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import hu.gszigligeti.swamp.models.SwampCloudToken;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.repositories.SwampCloudTokenRepository;
import hu.gszigligeti.swamp.services.SwampCloudMessagingService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Service implementation class for GCM notifications and device registration
 * @author Gergely Szigligeti
 */
@Service
public class SwampCloudMessagingServiceImpl implements SwampCloudMessagingService {
    @Autowired
    private SwampCloudTokenRepository cloudTokenRepository;

    @Autowired
    private SwampUserService userService;

    @Value("${spring.gcm.api-key}")
    private String gcmServerApiKey;

    @Override
    @PreAuthorize("isAuthenticated() and @SwampSecurity.accountIsModifiable(#userId)")
    public SwampCloudToken registerDeviceToSwampUser(String userId, String registrationToken) {
        SwampUser selectedUser = this.userService.findSwampUserById(userId);
        SwampCloudToken.CloudTokenKey key = new SwampCloudToken.CloudTokenKey(selectedUser, registrationToken);

        return this.cloudTokenRepository.save(new SwampCloudToken(key));
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public Boolean sendNotificationToDevices(List<String> registrationIds, Map<String, String> payload) throws IOException {
        if (registrationIds == null || registrationIds.isEmpty()) {
            return false;
        }

        Sender gcmSender = new Sender(this.gcmServerApiKey);
        Message.Builder notificationMessage = new Message.Builder();
        payload.forEach(notificationMessage::addData);
        MulticastResult result = gcmSender.sendNoRetry(notificationMessage.build(), registrationIds);

        return result.getSuccess() == registrationIds.size();
    }

    @Override
    @PreAuthorize("isAuthenticated() and @SwampSecurity.accountIsModifiable(#userId)")
    public void unregisterDeviceFromSwampUser(String userId, String registrationToken) {
        this.cloudTokenRepository.delete(this.cloudTokenRepository.findByCloudTokenKey(userId, registrationToken));
    }
}
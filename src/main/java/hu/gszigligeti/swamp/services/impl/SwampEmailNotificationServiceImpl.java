package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.services.SwampEmailNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Service implementation class for email notifications (activation, password reset, etc.)
 * @author Gergely Szigligeti
 */
@Service
public class SwampEmailNotificationServiceImpl implements SwampEmailNotificationService {
    @Autowired
    private JavaMailSenderImpl mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private MailProperties mailProperties;

    @Override
    public void sendAccountActivationEmail(final SwampUser swampUser, final String host) {
        final Context thymeleafContext = new Context();
        thymeleafContext.setVariable("server_host", host);
        thymeleafContext.setVariable("user", swampUser);
        final String subject = swampUser.getLanguage().equals("en") ? "Welcome to Swamp!" : "Üdvözlünk a Swamp-nél!";
        final String body = this.templateEngine.process("activation_email_" + swampUser.getLanguage(), thymeleafContext);

        this.sendEmail(swampUser.getEmailAddress(), subject, body);
    }

    @Override
    public void sendAccountPasswordResetEmail(final SwampUser swampUser, String host) {
        final Context thymeleafContext = new Context();
        thymeleafContext.setVariable("server_host", host);
        thymeleafContext.setVariable("user", swampUser);
        final String subject = swampUser.getLanguage().equals("en") ? "Swamp password reset" : "Swamp jelszócsere";
        final String body = this.templateEngine.process("password_reset_email_" + swampUser.getLanguage(), thymeleafContext);

        this.sendEmail(swampUser.getEmailAddress(), subject, body);
    }

    /**
     * Generic email-sender method used by the specific notification types
     * @param recipient The recipient of the message
     * @param subject The subject of the message
     * @param content The HTML content of the email body
     */
    private void sendEmail(final String recipient, final String subject, final String content) {
        MimeMessagePreparator mimeMessagePreparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws MessagingException {
                MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage);

                mimeMessageHelper.setFrom(mailProperties.getUsername());
                mimeMessageHelper.setTo(recipient);
                mimeMessageHelper.setSubject(subject);
                mimeMessageHelper.setText(content, true);
            }
        };

        this.mailSender.send(mimeMessagePreparator);
    }
}
package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.controllers.dto.SwampGroupDetails;
import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.repositories.SwampGroupRepository;
import hu.gszigligeti.swamp.services.SwampGroupService;
import hu.gszigligeti.swamp.services.SwampMembershipService;
import hu.gszigligeti.swamp.services.SwampUserService;
import hu.gszigligeti.swamp.utilities.SwampMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service implementation for group-related backend operations (create, search, update, etc.)
 * @author Gergely Szigligeti
 */
@Service
@Transactional
public class SwampGroupServiceImpl implements SwampGroupService {
    @Autowired
    private SwampGroupRepository groupRepository;

    @Autowired
    private SwampUserService userService;

    @Autowired
    private SwampMembershipService membershipService;

    @Autowired
    private SwampMessageSource messageSource;

    @Override
    public SwampGroup createSwampGroup(SwampGroupDetails groupOverview) {
        final SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        final SwampGroup groupToBeCreated = this.groupRepository.save(SwampGroup.fromDTO(groupOverview));

        this.membershipService.addSwampGroupSuperuser(groupToBeCreated, authenticatedUser);

        return groupToBeCreated;
    }

    @Override
    @PostFilter("@SwampSecurity.hasReadPermission(filterObject)")
    public Set<SwampGroup> findEverySwampGroup() {
        return this.groupRepository.findAll().stream().collect(Collectors.toSet());
    }

    @Override
    @PostAuthorize("@SwampSecurity.hasReadPermission(returnObject)")
    public SwampGroup findSwampGroupById(String groupId) {
        return this.groupRepository.findOne(groupId);
    }

    @Override
    @PreAuthorize("@SwampSecurity.hasPrivilegedPermission(#groupOverview.groupId)")
    public SwampGroup updateSwampGroup(SwampGroupDetails groupOverview) {
        SwampGroup fetchedGroup = this.groupRepository.findOne(groupOverview.getGroupId());

        fetchedGroup.setGroupName(groupOverview.getGroupName());
        fetchedGroup.setDescription(groupOverview.getDescription());
        fetchedGroup.setPublic(groupOverview.getPublic());

        return this.groupRepository.save(fetchedGroup);
    }

    @Override
    @PreAuthorize("@SwampSecurity.hasSuperuserPermission(#groupId)")
    public void deleteSwampGroupById(String groupId) {
        this.groupRepository.delete(groupId);
    }
}
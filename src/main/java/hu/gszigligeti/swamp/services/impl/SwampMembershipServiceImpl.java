package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.constants.SwampGroupPermissions;
import hu.gszigligeti.swamp.constants.SwampGroupRoles;
import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.repositories.SwampGroupMembershipRepository;
import hu.gszigligeti.swamp.services.SwampGroupService;
import hu.gszigligeti.swamp.services.SwampMembershipService;
import hu.gszigligeti.swamp.services.SwampUserService;
import hu.gszigligeti.swamp.utilities.SwampMessageSource;
import hu.gszigligeti.swamp.utilities.SwampPermissionEvaluator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

import static hu.gszigligeti.swamp.constants.SwampGroupPermissions.SUPERUSER;

/**
 * Service implementation class for adding, removing and updating group memberships
 * @author Gergely Szigligeti
 */
@Service
@Transactional
public class SwampMembershipServiceImpl implements SwampMembershipService {
    @Autowired
    private SwampGroupMembershipRepository membershipRepository;

    @Autowired
    private SwampGroupService groupService;

    @Autowired
    private SwampUserService userService;

    @Autowired
    private SwampPermissionEvaluator permissionEvaluator;

    @Autowired
    private SwampMessageSource messageSource;

    @Override
    @PreAuthorize("@SwampSecurity.hasPrivilegedPermission(#groupId)")
    public SwampGroupMembership addSwampGroupMembership(String groupId, String userId, byte role) {
        SwampGroup group = this.groupService.findSwampGroupById(groupId);
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampUser member = this.userService.findSwampUserById(userId);
        Boolean userIsGroupOwner = this.permissionEvaluator.hasGroupPermission(authenticatedUser, group, SwampGroupPermissions.SUPERUSER);
        SwampGroupMembership.MembershipKey membershipKey = new SwampGroupMembership.MembershipKey(group, member);

        // Prevent user from adding another group owner
        if (role == SwampGroupRoles.OWNER || (!userIsGroupOwner && role == SwampGroupRoles.ADMINISTRATOR)) {
            throw new UnsupportedOperationException(this.messageSource.getExceptionMessage("SwampExceptions.operationNotAllowed"));
        }

        return this.membershipRepository.save(new SwampGroupMembership(membershipKey, role));
    }

    @Override
    public SwampGroupMembership addSwampGroupSuperuser(SwampGroup group, SwampUser member) {
        SwampGroupMembership.MembershipKey membershipKey = new SwampGroupMembership.MembershipKey(group, member);
        return group.getMembers().isEmpty() ? this.membershipRepository.save(new SwampGroupMembership(membershipKey, SwampGroupRoles.OWNER)) : null;
    }

    @Override
    public Set<SwampGroupMembership> findMembersOfSwampGroup(String groupId) {
        return this.groupService.findSwampGroupById(groupId).getMembers();
    }

    @Override
    public SwampGroupMembership findMemberById(String groupId, String userId) {
        Optional<SwampGroupMembership> membershipCandidate = this.groupService.findSwampGroupById(groupId).getMembers().stream().filter(membership -> {
            return membership.getMembershipKey().getUser().getId().equals(userId);
        }).findFirst();

        return membershipCandidate.isPresent() ? membershipCandidate.get() : null;
    }

    @Override
    @PreAuthorize("@SwampSecurity.hasPrivilegedPermission(#groupId)")
    public SwampGroupMembership updateSwampGroupMembership(String groupId, String userId, byte role) {
        SwampGroupMembership membership = this.membershipRepository.findByMembershipKey(groupId, userId);

        // Prevent user from adding another group owner
        if (role == SwampGroupRoles.OWNER) {
            throw new UnsupportedOperationException(this.messageSource.getExceptionMessage("SwampExceptions.ownerAlreadyExists"));
        }

        membership.setRole(role);

        return this.membershipRepository.save(membership);
    }

    @Override
    @PreAuthorize("@SwampSecurity.hasPrivilegedPermission(#groupId)")
    public void deleteSwampGroupMembership(String groupId, String userId) {
        SwampGroup group = this.groupService.findSwampGroupById(groupId);
        SwampUser member = this.userService.findSwampUserById(userId);
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampGroupMembership membership = this.membershipRepository.findByMembershipKey(groupId, userId);

        Boolean currentUserIsOwner = this.permissionEvaluator.hasGroupPermission(authenticatedUser, group, SUPERUSER);
        Boolean memberIsPrivileged = this.permissionEvaluator.hasGroupPermission(member, group, SwampGroupPermissions.PRIVILEGED);

        if (currentUserIsOwner || !memberIsPrivileged) {
            this.membershipRepository.delete(membership);
        }
    }
}
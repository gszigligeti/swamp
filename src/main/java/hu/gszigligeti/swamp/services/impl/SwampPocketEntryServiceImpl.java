package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.controllers.dto.SwampPocketEntryDetails;
import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampGroupPocket;
import hu.gszigligeti.swamp.models.SwampPocketEntry;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.repositories.SwampPocketEntryRepository;
import hu.gszigligeti.swamp.services.SwampCloudMessagingService;
import hu.gszigligeti.swamp.services.SwampPocketEntryService;
import hu.gszigligeti.swamp.services.SwampPocketService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service implementation class for adding, removing and updating pocket entries
 * @author Gergely Szigligeti
 */
@Service
@Transactional
public class SwampPocketEntryServiceImpl implements SwampPocketEntryService {
    @Autowired
    private SwampPocketEntryRepository pocketEntryRepository;

    @Autowired
    private SwampPocketService pocketService;

    @Autowired
    private SwampCloudMessagingService cloudMessagingService;

    @Autowired
    private SwampUserService userService;

    @Override
    @PreAuthorize("@SwampSecurity.pocketEntryIsCreatable(#entryDetails.parentPocket)")
    public SwampPocketEntry addPocketEntry(SwampPocketEntryDetails entryDetails) throws IOException {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampAbstractPocket parentPocket = this.pocketService.findSwampPocketById(entryDetails.getParentPocket());
        SwampPocketEntry pocketEntry = new SwampPocketEntry();

        pocketEntry.setEntryName(entryDetails.getEntryName());
        pocketEntry.setDescription(entryDetails.getDescription());
        pocketEntry.setAmount(entryDetails.getAmount());
        pocketEntry.setParentPocket(parentPocket);

        this.pocketService.addMoneyToPocket(parentPocket, entryDetails.getAmount());

        if (parentPocket instanceof SwampGroupPocket) {
            Set<SwampGroupMembership> memberships = ((SwampGroupPocket) parentPocket).getPocketOwner().getMembers();
            Map<String, String> messagePayload = new HashMap<String, String>();
            List<String> tokenList = new ArrayList<String>();

            messagePayload.put("first_name", authenticatedUser.getFirstName());
            messagePayload.put("last_name", authenticatedUser.getLastName());
            messagePayload.put("pocket_name", parentPocket.getPocketName());

            memberships.stream().filter(membership -> !membership.getMembershipKey().getUser().equals(authenticatedUser)).forEach(membership -> {
                tokenList.addAll(membership.getMembershipKey().getUser().getDeviceTokens().stream().map(t -> t.getCloudTokenKey().getDeviceToken()).collect(Collectors.toList()));
            });

            this.cloudMessagingService.sendNotificationToDevices(tokenList, messagePayload);
        }

        return this.pocketEntryRepository.save(pocketEntry);
    }

    @Override
    @PostFilter("@SwampSecurity.pocketEntryIsAccessible(filterObject)")
    public Set<SwampPocketEntry> findEntriesOfOwner(String pocketEntryOwnerId) {
        SwampAbstractPocket parentPocket = this.pocketService.findSwampPocketById(pocketEntryOwnerId);
        return this.pocketEntryRepository.findAllByParentPocket(parentPocket);
    }

    @Override
    @PostFilter("@SwampSecurity.pocketEntryIsAccessible(filterObject)")
    public Set<SwampPocketEntry> findEveryPocketEntry() {
        return this.pocketEntryRepository.findAll().stream().collect(Collectors.toSet());
    }

    @Override
    @PostAuthorize("@SwampSecurity.pocketEntryIsAccessible(returnObject)")
    public SwampPocketEntry findPocketEntryById(String pocketEntryId) {
        return this.pocketEntryRepository.findOne(pocketEntryId);
    }

    @Override
    @PreAuthorize("@SwampSecurity.pocketEntryIsModifiable(#entryDetails.entryId)")
    public SwampPocketEntry updatePocketEntry(SwampPocketEntryDetails entryDetails) {
        SwampPocketEntry pocketEntry = this.pocketEntryRepository.findOne(entryDetails.getEntryId());

        this.pocketService.addMoneyToPocket(pocketEntry.getParentPocket(), entryDetails.getAmount() - pocketEntry.getAmount());

        pocketEntry.setEntryName(entryDetails.getEntryName());
        pocketEntry.setDescription(entryDetails.getDescription());
        pocketEntry.setAmount(entryDetails.getAmount());

        return this.pocketEntryRepository.save(pocketEntry);
    }

    @Override
    @PreAuthorize("@SwampSecurity.pocketEntryIsModifiable(#pocketEntryId)")
    public void deletePocketEntry(String pocketEntryId) {
        SwampPocketEntry pocketEntry = this.pocketEntryRepository.findOne(pocketEntryId);

        this.pocketService.withdrawMoneyFromPocket(pocketEntry.getParentPocket(), pocketEntry.getAmount());
        this.pocketEntryRepository.delete(pocketEntry);
    }
}
package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.constants.SwampPocketTypes;
import hu.gszigligeti.swamp.controllers.dto.SwampPocketDetails;
import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampGroupPocket;
import hu.gszigligeti.swamp.models.SwampUserPocket;
import hu.gszigligeti.swamp.repositories.SwampAbstractPocketRepository;
import hu.gszigligeti.swamp.services.SwampGroupService;
import hu.gszigligeti.swamp.services.SwampPocketService;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.stream.Collectors;

import static hu.gszigligeti.swamp.utilities.SwampStreamUtilities.pocketOwnershipFilter;

/**
 * Service implementation class for adding, removing and updating pockets
 * @author Gergely Szigligeti
 */
@Service
@Transactional
public class SwampPocketServiceImpl implements SwampPocketService {
    @Autowired
    private SwampUserService userService;

    @Autowired
    private SwampGroupService groupService;

    @Autowired
    private SwampAbstractPocketRepository pocketRepository;

    @Override
    @PreAuthorize("@SwampSecurity.pocketIsCreatable(#ownerId, #pocketType)")
    public SwampAbstractPocket createNewSwampPocket(String ownerId, String pocketType, SwampPocketDetails pocketDetails) {
        SwampAbstractPocket abstractPocket = pocketType.toUpperCase().equals(SwampPocketTypes.GROUP) ? new SwampGroupPocket() : new SwampUserPocket();

        abstractPocket.setPocketName(pocketDetails.getPocketName());
        abstractPocket.setDescription(pocketDetails.getDescription());
        abstractPocket.setInitialValue(pocketDetails.getBalance());
        abstractPocket.setBalance(pocketDetails.getBalance());
        abstractPocket.setCurrency(pocketDetails.getCurrency());

        if (abstractPocket instanceof SwampGroupPocket) {
            ((SwampGroupPocket) abstractPocket).setPocketOwner(this.groupService.findSwampGroupById(ownerId));
        }

        if (abstractPocket instanceof SwampUserPocket) {
            ((SwampUserPocket) abstractPocket).setPocketOwner(this.userService.findSwampUserById(ownerId));
        }

        return this.pocketRepository.save(abstractPocket);
    }

    @Override
    @PostAuthorize("@SwampSecurity.pocketIsAccessible(returnObject)")
    public SwampAbstractPocket findSwampPocketById(String pocketId) {
        return this.pocketRepository.findOne(pocketId);
    }

    @Override
    @PreAuthorize("@SwampSecurity.pocketIsModifiable(#pocketDetails.pocketId)")
    public SwampAbstractPocket updateExistingSwampPocket(SwampPocketDetails pocketDetails) {
        SwampAbstractPocket fetchedPocket = this.pocketRepository.findOne(pocketDetails.getPocketId());

        fetchedPocket.setPocketName(pocketDetails.getPocketName());
        fetchedPocket.setDescription(pocketDetails.getDescription());
        fetchedPocket.setBalance(fetchedPocket.getBalance() + (pocketDetails.getInitialValue() - fetchedPocket.getInitialValue()));
        fetchedPocket.setInitialValue(pocketDetails.getInitialValue());
        fetchedPocket.setCurrency(pocketDetails.getCurrency());

        return this.pocketRepository.save(fetchedPocket);
    }

    @Override
    @PreAuthorize("@SwampSecurity.pocketIsModifiable(#pocketId)")
    public void deleteExistingSwampPocket(String pocketId) {
        this.pocketRepository.delete(pocketId);
    }

    @Override
    @PostFilter("@SwampSecurity.pocketIsAccessible(filterObject)")
    public Set<SwampAbstractPocket> findEverySwampPocket() {
        return this.pocketRepository.findAll().stream().collect(Collectors.toSet());
    }

    @Override
    @PostFilter("@SwampSecurity.pocketIsAccessible(filterObject)")
    public Set<SwampAbstractPocket> findPocketsOfOwner(String pocketOwnerId) {
        return this.pocketRepository.findAll().stream().filter(pocketOwnershipFilter(pocketOwnerId)).collect(Collectors.toSet());
    }

    @Override
    public SwampAbstractPocket addMoneyToPocket(SwampAbstractPocket pocket, Integer amount) {
        pocket.setBalance(pocket.getBalance() + amount);
        return this.pocketRepository.save(pocket);
    }

    @Override
    public SwampAbstractPocket withdrawMoneyFromPocket(SwampAbstractPocket pocket, Integer amount) {
        pocket.setBalance(pocket.getBalance() - amount);
        return this.pocketRepository.save(pocket);
    }
}
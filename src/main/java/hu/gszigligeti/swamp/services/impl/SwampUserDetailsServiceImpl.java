package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.models.SwampUserDetails;
import hu.gszigligeti.swamp.repositories.SwampUserRepository;
import hu.gszigligeti.swamp.utilities.SwampAuthorityConverter;
import hu.gszigligeti.swamp.utilities.SwampMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class which is responsible for authenticating the users by email address and password combination
 * @author Gergely Szigligeti
 */
@Service
public class SwampUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private SwampUserRepository userRepository;

    @Autowired
    private SwampMessageSource messageSource;

    /**
     * Method for the authentication process
     * @param emailAddress The email address of the user, which we would like to authenticate
     * @return The Spring Security user context (principal)
     */
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String emailAddress) {
        // Fetch the user from the database by the email address
        SwampUser userToBeAuthenticated = this.userRepository.findOneByEmailAddress(emailAddress.toLowerCase());

        if (userToBeAuthenticated == null) {
            throw new UsernameNotFoundException(this.messageSource.getExceptionMessage("SwampExceptions.userNotFound"));
        }

        return new SwampUserDetails(userToBeAuthenticated, SwampAuthorityConverter.fromRoles(userToBeAuthenticated.getRoles()));
    }
}
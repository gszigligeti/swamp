package hu.gszigligeti.swamp.services.impl;

import hu.gszigligeti.swamp.constants.SwampUserRoles;
import hu.gszigligeti.swamp.controllers.dto.SwampAccountUpdateRequest;
import hu.gszigligeti.swamp.controllers.dto.SwampRegistrationRequest;
import hu.gszigligeti.swamp.exceptions.SwampRegistrationException;
import hu.gszigligeti.swamp.exceptions.SwampUserNotFoundException;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.repositories.SwampUserRepository;
import hu.gszigligeti.swamp.services.SwampEmailNotificationService;
import hu.gszigligeti.swamp.services.SwampUserService;
import hu.gszigligeti.swamp.utilities.SwampMessageSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service implementation for user-related backend operations (registration, account locking, etc.)
 * @author Gergely Szigligeti
 */
@Service
@Transactional
public class SwampUserServiceImpl implements SwampUserService {
    @Autowired
    private SwampUserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SwampEmailNotificationService notificationService;

    @Autowired
    private SwampMessageSource messageSource;

    @Override
    public SwampUser registerNewUser(SwampRegistrationRequest registrationRequest, HttpServletRequest servletRequest) throws SwampRegistrationException {
        final SwampUser userToBeRegistered = SwampUser.fromDTO(registrationRequest);
        final String serverUrl = String.format("%s://%s:%s", servletRequest.getScheme(), servletRequest.getServerName(), servletRequest.getServerPort());

        // Check if the given e-mail address is already registered
        if (this.userRepository.findOneByEmailAddress(userToBeRegistered.getEmailAddress()) != null) {
            throw new SwampRegistrationException(this.messageSource.getExceptionMessage("SwampExceptions.emailExists"));
        }

        // Check if the password satisfies the Swamp password standards
        if (!userToBeRegistered.getPassword().matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
            throw new SwampRegistrationException(this.messageSource.getExceptionMessage("SwampExceptions.passwordNotSecure"));
        }

        // Salt and hash the user's password before registration then initialize the activation code
        userToBeRegistered.setPassword(this.passwordEncoder.encode(userToBeRegistered.getPassword()));
        userToBeRegistered.setRoles(Collections.singleton(SwampUserRoles.USER));
        userToBeRegistered.setProfilePicture("no_profile_image.png");
        userToBeRegistered.setActivationCode(UUID.randomUUID().toString());

        this.notificationService.sendAccountActivationEmail(userToBeRegistered, serverUrl);

        return this.userRepository.save(userToBeRegistered);
    }

    @Override
    public SwampUser activateUserAccount(String activationCode) {
        // Search for the user by the activation code
        final SwampUser userAccountToBeActivated = this.userRepository.findOneByActivationCode(activationCode);

        if (userAccountToBeActivated == null) {
            throw new SwampUserNotFoundException(this.messageSource.getExceptionMessage("SwampExceptions.invalidCode"));
        }

        // If there's a user with this activation code, then turn on the flag and erase the activation code
        userAccountToBeActivated.setActivated(true);
        userAccountToBeActivated.setActivationCode(null);

        return this.userRepository.save(userAccountToBeActivated);
    }

    @Override
    public SwampUser sendPasswordResetEmail(String emailAddress, HttpServletRequest servletRequest) {
        // Search for the user by its email address
        final SwampUser userWithForgottenPassword = this.userRepository.findOneByEmailAddress(emailAddress);
        final String serverUrl = String.format("%s://%s:%s", servletRequest.getScheme(), servletRequest.getServerName(), servletRequest.getServerPort());

        if (userWithForgottenPassword == null) {
            throw new SwampUserNotFoundException(this.messageSource.getExceptionMessage("SwampExceptions.userNotFound"));
        }

        if (!userWithForgottenPassword.getActivated()) {
            throw new SwampUserNotFoundException(this.messageSource.getExceptionMessage("SwampExceptions.accountNotActivated"));
        }

        final Calendar tokenValidityCalendar = Calendar.getInstance();
        tokenValidityCalendar.add(Calendar.MINUTE, 30);

        // If the user has been found, then generate the reset token and initialize its validity
        userWithForgottenPassword.setPasswordResetToken(UUID.randomUUID().toString());
        userWithForgottenPassword.setPasswordResetTokenExpiration(tokenValidityCalendar.getTime());
        this.notificationService.sendAccountPasswordResetEmail(userWithForgottenPassword, serverUrl);

        return this.userRepository.save(userWithForgottenPassword);
    }

    @Override
    public SwampUser resetForgottenPassword(String passwordResetToken, String updatedPassword) {
        // Search for the user by the secret token
        final SwampUser userWithForgottenPassword = this.userRepository.findOneByPasswordResetToken(passwordResetToken);

        if (userWithForgottenPassword == null || userWithForgottenPassword.getPasswordResetTokenExpiration().before(new Date())) {
            throw new SwampUserNotFoundException(this.messageSource.getExceptionMessage("SwampExceptions.invalidToken"));
        }

        // Check if the user has already activated the account
        if (!userWithForgottenPassword.getActivated()) {
            throw new RuntimeException(this.messageSource.getExceptionMessage("SwampExceptions.accountNotActivated"));
        }

        // Check if the new password satisfies the Swamp password standards
        if (!updatedPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
            throw new RuntimeException(this.messageSource.getExceptionMessage("SwampExceptions.passwordNotSecure"));
        }

        // If there's a user with this token, then update the password and erase the secret token and its validity
        userWithForgottenPassword.setPasswordResetToken(null);
        userWithForgottenPassword.setPasswordResetTokenExpiration(null);
        userWithForgottenPassword.setPassword(this.passwordEncoder.encode(updatedPassword));

        return this.userRepository.save(userWithForgottenPassword);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public SwampUser getCurrentlyAuthenticatedUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return this.userRepository.findOneByEmailAddress(authentication.getName());
    }

    @Override
    @PreAuthorize("isAuthenticated() and @SwampSecurity.accountIsModifiable(#userId)")
    public SwampUser updateSwampUserAccount(String userId, SwampAccountUpdateRequest updateRequest) {
        SwampUser userToUpdate = this.userRepository.findOne(userId);

        userToUpdate.setFirstName(updateRequest.getFirstName() == null ? userToUpdate.getFirstName() : updateRequest.getFirstName());
        userToUpdate.setLastName(updateRequest.getLastName() == null ? userToUpdate.getLastName() : updateRequest.getLastName());
        userToUpdate.setEmailAddress(updateRequest.getEmailAddress() == null ? userToUpdate.getEmailAddress() : updateRequest.getEmailAddress());

        if (updateRequest.getNewPassword() != null && !updateRequest.getNewPassword().isEmpty()) {
            String encodedNewPassword = this.passwordEncoder.encode(updateRequest.getNewPassword());

            if (!this.passwordEncoder.matches(updateRequest.getCurrentPassword(), userToUpdate.getPassword())) {
                throw new RuntimeException(this.messageSource.getExceptionMessage("SwampExceptions.passwordVerificationError"));
            }

            userToUpdate.setPassword(encodedNewPassword);
        }

        userToUpdate.setPublic(updateRequest.getPublic() == null ? userToUpdate.getPublic() : updateRequest.getPublic());
        userToUpdate.setLanguage(updateRequest.getLanguage() == null ? userToUpdate.getLanguage() : updateRequest.getLanguage());

        return this.userRepository.save(userToUpdate);
    }

    @Override
    @PreAuthorize("isAuthenticated() and @SwampSecurity.accountIsModifiable(#userId)")
    public void deleteSwampUserAccount(String userId, String password) {
        SwampUser userToDelete = this.userRepository.findOne(userId);
        SwampUser authenticatedUser = this.getCurrentlyAuthenticatedUser();
        Boolean isAdmin = authenticatedUser.getRoles().contains(SwampUserRoles.ADMINISTRATOR);

        if (!isAdmin && !this.passwordEncoder.matches(password, userToDelete.getPassword())) {
            throw new RuntimeException(this.messageSource.getExceptionMessage("SwampExceptions.passwordMatchError"));
        }

        this.userRepository.delete(userId);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PostAuthorize("@SwampSecurity.accountIsAccessible(returnObject)")
    public SwampUser findSwampUserById(String userId) {
        return this.userRepository.findOne(userId);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    @PostFilter("@SwampSecurity.accountIsAccessible(filterObject)")
    public Set<SwampUser> findEverySwampUser() {
        return this.userRepository.findAll().stream().collect(Collectors.toSet());
    }
}
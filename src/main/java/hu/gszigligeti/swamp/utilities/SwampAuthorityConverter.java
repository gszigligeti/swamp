package hu.gszigligeti.swamp.utilities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Utility class to easily convert roles and authorities back and forth
 * @author Gergely Szigligeti
 */
public class SwampAuthorityConverter {
    /**
     * Method responsible for converting a set of strings into Spring authorities
     * @param userRoles The list of roles we want to convert
     * @return The converted list of authorities
     */
    public static Set<GrantedAuthority> fromRoles(Set<String> userRoles) {
        return userRoles.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
    }

    /**
     * Method responsible for converting a set of Spring authorities into strings
     * @param authorities The list of authorities we want to convert
     * @return The converted list of roles
     */
    public static Set<String> toRoles(Set<GrantedAuthority> authorities) {
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }
}
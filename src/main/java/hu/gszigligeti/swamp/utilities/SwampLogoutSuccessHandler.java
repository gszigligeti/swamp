package hu.gszigligeti.swamp.utilities;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Utility class for preventing redirection after logout
 * @author Gergely Szigligeti
 */
@Component
public class SwampLogoutSuccessHandler implements LogoutSuccessHandler {
    /**
     * Method for sending back a response after successful logout
     * @param request The injected HTTP request object
     * @param response The injected HTTP response object
     * @param authentication The authentication context of the logged out user
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
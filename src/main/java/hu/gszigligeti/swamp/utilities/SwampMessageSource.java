package hu.gszigligeti.swamp.utilities;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

/**
 * Helper class to easily convert exceptions messages into translation keys
 * @author Gergely Szigligeti
 */
@Component
public class SwampMessageSource implements MessageSourceAware {
    private MessageSourceAccessor messageSourceAccessor;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSourceAccessor = new MessageSourceAccessor(messageSource);
    }

    /**
     * Method for getting the message with the given identifier
     * @param messageIdentifier The key part of the message key-value pair
     * @return The value part of the message key-value pair
     */
    public String getExceptionMessage(String messageIdentifier) {
        return this.messageSourceAccessor.getMessage(messageIdentifier);
    }
}
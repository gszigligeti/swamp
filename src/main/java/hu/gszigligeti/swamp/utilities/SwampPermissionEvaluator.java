package hu.gszigligeti.swamp.utilities;

import hu.gszigligeti.swamp.constants.SwampGroupPermissions;
import hu.gszigligeti.swamp.constants.SwampPocketTypes;
import hu.gszigligeti.swamp.constants.SwampUserRoles;
import hu.gszigligeti.swamp.exceptions.SwampGroupNotFoundException;
import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampGroup;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampGroupPocket;
import hu.gszigligeti.swamp.models.SwampPocketEntry;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.models.SwampUserPocket;
import hu.gszigligeti.swamp.repositories.SwampAbstractPocketRepository;
import hu.gszigligeti.swamp.repositories.SwampGroupRepository;
import hu.gszigligeti.swamp.repositories.SwampPocketEntryRepository;
import hu.gszigligeti.swamp.services.SwampUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Set;

import static hu.gszigligeti.swamp.utilities.SwampStreamUtilities.groupMembershipFilter;
import static hu.gszigligeti.swamp.utilities.SwampStreamUtilities.groupPermissionFilter;

/**
 * Component class for authorizing interactions within groups
 * @author Gergely Szigligeti
 */
@Component("SwampSecurity")
public class SwampPermissionEvaluator {
    @Autowired
    private SwampUserService userService;

    @Autowired
    private SwampGroupRepository groupRepository;

    @Autowired
    private SwampAbstractPocketRepository pocketRepository;

    @Autowired
    private SwampPocketEntryRepository pocketEntryRepository;

    @Autowired
    private SwampMessageSource messageSource;

    public Boolean accountIsAccessible(SwampUser swampUser) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        Boolean isAdmin = authenticatedUser.getRoles().contains(SwampUserRoles.ADMINISTRATOR);
        return isAdmin || authenticatedUser.getId().equals(swampUser.getId()) || swampUser.getPublic();
    }

    public Boolean accountIsModifiable(String userId) {
        SwampUser checkedUser = this.userService.findSwampUserById(userId);
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        Boolean isAdmin = authenticatedUser.getRoles().contains(SwampUserRoles.ADMINISTRATOR);
        return isAdmin || authenticatedUser.getId().equals(checkedUser.getId());
    }

    // ~~~~ GROUP SECURITY ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    /**
     * Security method for deciding whether the user has a given permission in a group
     * @param user The user's entity
     * @param group The group that we'd like to check
     * @param permission The permission that the user will be validated against
     * @return True if the user has sufficient privileges, otherwise false
     */
    public Boolean hasGroupPermission(SwampUser user, SwampGroup group, byte permission) throws SwampGroupNotFoundException {
        Boolean isAdmin = user.getRoles().contains(SwampUserRoles.ADMINISTRATOR);
        Boolean publicAccess = (permission == SwampGroupPermissions.READ) && group.getPublic();
        Set<SwampGroupMembership> memberships = group == null ? null : group.getMembers();

        return isAdmin || publicAccess || (memberships != null && memberships.stream().filter(groupMembershipFilter(user).and(groupPermissionFilter(permission))).count() > 0);
    }

    public Boolean hasReadPermission(SwampGroup group) {
        return this.hasGroupPermission(this.userService.getCurrentlyAuthenticatedUser(), group, SwampGroupPermissions.READ);
    }

    public Boolean hasWritePermission(String groupId) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampGroup checkedGroup = this.groupRepository.findOne(groupId);

        return this.hasGroupPermission(authenticatedUser, checkedGroup, SwampGroupPermissions.WRITE);
    }

    public Boolean hasPrivilegedPermission(String groupId) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampGroup checkedGroup = this.groupRepository.findOne(groupId);

        return this.hasGroupPermission(authenticatedUser, checkedGroup, SwampGroupPermissions.PRIVILEGED);
    }

    public Boolean hasSuperuserPermission(String groupId) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();
        SwampGroup checkedGroup = this.groupRepository.findOne(groupId);

        return this.hasGroupPermission(authenticatedUser, checkedGroup, SwampGroupPermissions.SUPERUSER);
    }

    // ~~~~ POCKET SECURITY ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public Boolean pocketIsAccessible(String pocketId) {
        return this.pocketIsAccessible(this.pocketRepository.findOne(pocketId));
    }

    public Boolean pocketIsAccessible(SwampAbstractPocket abstractPocket) {
        return this.hasPocketPermission(abstractPocket, SwampGroupPermissions.READ);
    }

    public Boolean pocketIsCreatable(String ownerId, String pocketType) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();

        if (pocketType.equals(SwampPocketTypes.USER)) {
            return ownerId.equals(authenticatedUser.getId());
        }

        if (pocketType.equals(SwampPocketTypes.GROUP)) {
            SwampGroup ownerGroup = this.groupRepository.findOne(ownerId);
            return this.hasGroupPermission(authenticatedUser, ownerGroup, SwampGroupPermissions.PRIVILEGED);
        }

        return false;
    }

    public Boolean pocketIsModifiable(String pocketId) {
        return this.pocketIsModifiable(this.pocketRepository.findOne(pocketId));
    }

    public Boolean pocketIsModifiable(SwampAbstractPocket abstractPocket) {
        return this.hasPocketPermission(abstractPocket, SwampGroupPermissions.PRIVILEGED);
    }

    public Boolean hasPocketPermission(SwampAbstractPocket abstractPocket, byte permission) {
        SwampUser authenticatedUser = this.userService.getCurrentlyAuthenticatedUser();

        if (abstractPocket instanceof SwampUserPocket) {
            return ((SwampUserPocket) abstractPocket).getPocketOwner().equals(authenticatedUser);
        }

        if (abstractPocket instanceof SwampGroupPocket) {
            SwampGroup ownerGroup = ((SwampGroupPocket) abstractPocket).getPocketOwner();
            return this.hasGroupPermission(authenticatedUser, ownerGroup, permission);
        }

        return false;
    }

    // ~~~~ POCKET ENTRY SECURITY ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    public Boolean pocketEntryIsAccessible(String pocketEntryId) {
        return this.pocketEntryIsAccessible(this.pocketEntryRepository.findOne(pocketEntryId));
    }

    public Boolean pocketEntryIsAccessible(SwampPocketEntry pocketEntry) {
        return this.hasPocketPermission(pocketEntry.getParentPocket(), SwampGroupPermissions.READ);
    }

    public Boolean pocketEntryIsCreatable(String pocketId) {
        return this.hasPocketPermission(this.pocketRepository.findOne(pocketId), SwampGroupPermissions.WRITE);
    }

    public Boolean pocketEntryIsModifiable(String pocketEntryId) {
        return this.pocketEntryIsModifiable(this.pocketEntryRepository.findOne(pocketEntryId));
    }

    public Boolean pocketEntryIsModifiable(SwampPocketEntry pocketEntry) {
        return this.hasPocketPermission(pocketEntry.getParentPocket(), SwampGroupPermissions.WRITE);
    }
}
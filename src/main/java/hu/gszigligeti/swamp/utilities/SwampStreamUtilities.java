package hu.gszigligeti.swamp.utilities;

import hu.gszigligeti.swamp.models.SwampAbstractPocket;
import hu.gszigligeti.swamp.models.SwampGroupMembership;
import hu.gszigligeti.swamp.models.SwampGroupPocket;
import hu.gszigligeti.swamp.models.SwampUser;
import hu.gszigligeti.swamp.models.SwampUserPocket;

import java.util.function.Predicate;

/**
 * Predicates and stream utility functions
 * @author Gergely Szigligeti
 */
public class SwampStreamUtilities {
    public static final Predicate<SwampGroupMembership> groupMembershipFilter(SwampUser authenticatedUser) {
        return p -> p.getMembershipKey().getUser().equals(authenticatedUser);
    }

    public static final Predicate<SwampGroupMembership> groupPermissionFilter(byte permission) {
        return p -> (p.getRole() & permission) == p.getRole();
    }

    public static final Predicate<SwampAbstractPocket> pocketOwnershipFilter(String ownerId) {
        return p -> {
            if (p instanceof SwampUserPocket) {
                return ((SwampUserPocket) p).getPocketOwner().getId().equals(ownerId);
            }

            return p instanceof SwampGroupPocket && ((SwampGroupPocket) p).getPocketOwner().getId().equals(ownerId);
        };
    }
}
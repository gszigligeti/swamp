/**
 * Module definition file, which injects every submodule into the main application module
 * @author Gergely Szigligeti
 */
var SwampConfiguration = angular.module("SwampConfiguration", ["ui.router", "angular.translate", "ngCookies"]);
var SwampControllers   = angular.module("SwampControllers", ["ui.router", "ui.select", "ui.chart", "ngSanitize", "ngMessages"]);
var SwampDirectives    = angular.module("SwampDirectives", []);
var SwampFilters       = angular.module("SwampFilters", []);
var SwampServices      = angular.module("SwampServices", []);

var SwampApplication   = angular.module("SwampApplication", ["SwampConfiguration", "SwampControllers", "SwampDirectives", "SwampFilters", "SwampServices"]);
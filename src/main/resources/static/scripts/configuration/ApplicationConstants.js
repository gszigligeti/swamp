/**
 * Configuration file for constant values used through the entire application
 * @author Gergely Szigligeti
 */
SwampConfiguration.constant("SwampConstants", {
    oauth: {
        clientId: "077BB83D76FD811663CE",
        passwordGrantType: "password",
        refreshTokenGrantType: "refresh_token"
    },
    roleMappings: {
        MEMBER: parseInt("0001", 2),
        EDITOR: parseInt("0010", 2),
        ADMIN:  parseInt("0100", 2),
        OWNER:  parseInt("1000", 2)
    },
    groupPermissions: {
        READ: ["MEMBER", "EDITOR", "ADMIN", "OWNER"],
        WRITE: ["EDITOR", "ADMIN", "OWNER"],
        PRIVILEGED: ["ADMIN", "OWNER"],
        SUPERUSER: ["OWNER"]
    }
});

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ~~ Taken from Github - https://gist.github.com/gre/1650294 ~~~~~
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
SwampConfiguration.constant("EasingFunctions", {
    linear: function (time) {
        return time;
    },
    easeInQuad: function (time) {
        return Math.pow(time, 2);
    },
    easeOutQuad: function (time) {
        return time * (2 - time);
    },
    easeInOutQuad: function (time) {
        return time < 0.5 ? (2 * Math.pow(time, 2)) : (-1 + (4 - 2 * time) * time);
    },
    easeInCubic: function (time) {
        return Math.pow(time, 3);
    },
    easeOutCubic: function (time) {
        return (--time) * Math.pow(time, 2) + 1;
    },
    easeInOutCubic: function (time) {
        return time < 0.5 ? (4 * Math.pow(time, 3)) : ((time - 1) * Math.pow(2 * time - 2, 2) + 1);
    },
    easeInQuart: function (time) {
        return Math.pow(time, 4);
    },
    easeOutQuart: function (time) {
        return 1 - (--time) * Math.pow(time, 3);
    },
    easeInOutQuart: function (time) {
        return time < 0.5 ? (8 * Math.pow(time, 4)) : (1 - 8 * (--time) * Math.pow(time, 3));
    },
    easeInQuint: function (time) {
        return Math.pow(time, 5);
    },
    easeOutQuint: function (time) {
        return 1 + (--time) * Math.pow(time, 4);
    },
    easeInOutQuint: function (time) {
        return time < 0.5 ? (16 * Math.pow(time, 5)) : (1 + 16 * (--t) * Math.pow(time, 4));
    }
});
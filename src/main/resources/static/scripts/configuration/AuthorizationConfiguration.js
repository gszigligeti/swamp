/**
 * Configuration file for setting up the secured routes of the application
 * @author Gergely Szigligeti
 */
var AuthorizationConfiguration = function ($rootScope, $state, IdentityService) {
    var redirectRouting = function (stateName, stateParams) {
        $state.go(stateName, stateParams, {notify: false}).then(function (state) {
            $rootScope.titleKey = state.title;
            $rootScope.$broadcast("$stateChangeSuccess", state, null);
        });
    };

    // Global event listener for state changes
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        // Stop the event processing and wait for async result
        event.preventDefault();

        return IdentityService.resolveIdentity().then(function (identityInfo) {
            var nextStateIsPublic = toState.name.indexOf("public") == 0;

            // If the user is authenticated and wants to access a public route
            if (nextStateIsPublic && identityInfo.authenticated) {
                return redirectRouting("protected.dashboard", null);
            }

            // If the user is not authenticated and wants to access a protected route
            if (!nextStateIsPublic && !identityInfo.authenticated) {
                return redirectRouting("public.login", null);
            }

            redirectRouting(toState.name, toParams);
        });
    });
};

// Minification-safe dependency injection
AuthorizationConfiguration.$inject = ["$rootScope", "$state", "IdentityService"];
SwampConfiguration.run(AuthorizationConfiguration);
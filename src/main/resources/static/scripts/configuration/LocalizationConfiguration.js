/**
 * Configuration file for customizing angular-translate extension
 * @author Gergely Szigligeti
 */
var LocalizationConfiguration = function ($translateProvider) {
    // Setup the path of the localization files
    $translateProvider.useStaticFilesLoader({
        prefix: "languages/locale-",
        suffix: ".json"
    });

    // Other settings like storage type, default language, HTML sanitizing
    $translateProvider.preferredLanguage("en");
    $translateProvider.storageKey("language_key");
    $translateProvider.useLocalStorage();
};

// Minification-safe dependency injection
LocalizationConfiguration.$inject = ["$translateProvider"];
SwampConfiguration.config(LocalizationConfiguration);
/**
 * Configuration file for setting up the application URL routes
 * @author Gergely Szigligeti
 */
var RouteConfiguration = function ($stateProvider, $urlRouterProvider, $httpProvider) {
    // ~~~ PUBLIC ROUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $stateProvider.state("public", {
        abstract: true,
        template: "<div ui-view></div>"
    });

    $stateProvider.state("public.home", {
        url: "/home",
        controller: "HomeController",
        templateUrl: "templates/public/home.html",
        title: "TITLES.HOME"
    });

    $stateProvider.state("public.login", {
        url: "/login",
        controller: "LoginController",
        params: {
            accountActivation: null
        },
        templateUrl: "templates/public/login.html",
        title: "TITLES.LOGIN"
    });

    $stateProvider.state("public.register", {
        url: "/register",
        controller: "RegistrationController",
        templateUrl: "templates/public/registration.html",
        title: "TITLES.REGISTER"
    });

    $stateProvider.state("public.activate", {
        url: "/activate?code",
        controller: "ActivationController",
        templateUrl: "templates/public/activation.html",
        title: "TITLES.ACTIVATION"
    });

    $stateProvider.state("public.password_reset", {
        url: "/password/reset?token",
        controller: "PasswordResetController",
        templateUrl: "templates/public/password_reset.html",
        title: "TITLES.PASSWORD_RESET"
    });

    // ~~~ PROTECTED ROUTES ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $stateProvider.state("protected", {
        abstract: true,
        template: "<div ui-view></div>"
    });

    $stateProvider.state("protected.dashboard", {
        url: "/dashboard",
        controller: "DashboardController",
        templateUrl: "templates/protected/dashboard.html",
        title: "TITLES.DASHBOARD"
    });

    $stateProvider.state("protected.settings", {
        url: "/settings",
        controller: "SettingsPageController",
        templateUrl: "templates/protected/settings.html",
        params: {
            updateSuccess: null
        },
        title: "TITLES.SETTINGS"
    });

    $stateProvider.state("protected.finances", {
        url: "/finances",
        controller: "MyFinancesController",
        templateUrl: "templates/protected/my_finances.html",
        title: "TITLES.MY_FINANCES"
    });

    $stateProvider.state("protected.groups", {
        url: "/groups",
        controller: "GroupListController",
        templateUrl: "templates/protected/groups.html",
        params: {
            groupWasDeleted: null
        },
        title: "TITLES.GROUP_LIST"
    });

    $stateProvider.state("protected.group_details", {
        url: "/groups/:groupId",
        controller: "GroupDetailsController",
        templateUrl: "templates/protected/group_details.html",
        title: "TITLES.GROUP_DETAILS"
    });

    $stateProvider.state("protected.pocket_details", {
        url: "/pockets/:pocketId",
        controller: "PocketDetailsController",
        templateUrl: "templates/protected/pocket_details.html",
        title: "TITLES.POCKET_DETAILS"
    });

    // ~~~ DEFAULT ROUTE ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    $urlRouterProvider.otherwise(function ($injector, $location) {
        $injector.get("$state").go("public.home");
    });

    $httpProvider.interceptors.push("AuthorizationInterceptorService");
    $httpProvider.defaults.headers.common['Cache-Control'] = 'no-cache, no-store, must-revalidate';
};

// Minification-safe dependency injection
RouteConfiguration.$inject = ["$stateProvider", "$urlRouterProvider", "$httpProvider"];
SwampConfiguration.config(RouteConfiguration);
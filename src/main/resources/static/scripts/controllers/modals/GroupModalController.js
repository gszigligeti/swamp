/**
 * Application controller responsible for interacting with the group-related modals
 * @author Gergely Szigligeti
 */
var GroupModalController = function ($scope, $modal, GroupService, SelectedGroup) {
    $scope.selectedGroup = SelectedGroup;
    $scope.groupDetails = {
        public: false
    };

    $scope.createNewGroup = function () {
        if ($scope.createGroupForm.$valid) {
            GroupService.createSwampGroup($scope.groupDetails).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.updateSelectedGroup = function () {
        if ($scope.editGroupForm.$valid) {
            GroupService.updateSwampGroup($scope.selectedGroup).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.deleteSelectedGroup = function () {
        GroupService.deleteSwampGroupById($scope.selectedGroup.groupId).then(function (response) {
            $modal.close(response);
        });
    };

    $scope.dismissModal = function (delay) {
        $modal.dismiss(delay);
    };
};

// Minification-safe dependency injection
GroupModalController.$inject = ["$scope", "$modal", "GroupService", "SelectedGroup"];
SwampControllers.controller("GroupModalController", GroupModalController);
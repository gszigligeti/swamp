/**
 * Application controller responsible for interacting with the membership-related modals
 * @author Gergely Szigligeti
 */
var MemberModalController = function ($scope, $modal, MembershipService, UserService, SwampConstants, SelectedGroup, SelectedMember, CurrentMember, MemberList) {
    $scope.selectedGroup = SelectedGroup;
    $scope.selectedMember = SelectedMember;
    $scope.memberDetails = {};
    $scope.userList = [];

    if (SelectedMember == null) {
        UserService.findEverySwampUser().then(function (response) {
            angular.forEach(response.data, function (user) {
                var filter = MemberList.filter(function (member) {
                    return member.userId === user.userId;
                });

                if (filter.length == 0) {
                    $scope.userList.push(user);
                }
            });
        });
    }

    $scope.getAllowedUserRoles = function () {
        switch (CurrentMember.memberRole) {
            case "OWNER":
                return ["MEMBER", "EDITOR", "ADMIN"];
            case "ADMIN":
                return ["MEMBER", "EDITOR"];
        }
    };

    $scope.createMembership = function () {
        if ($scope.addMemberForm.$valid) {
            var userId = $scope.memberDetails.user.userId;
            var role = SwampConstants.roleMappings[$scope.memberDetails.role];

            MembershipService.addSwampGroupMembership($scope.selectedGroup.groupId, userId, role).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.updateMembership = function (role) {
        if ($scope.editMemberForm.$valid) {
            var userId = $scope.selectedMember.userId
            var role = SwampConstants.roleMappings[$scope.selectedMember.role];

            MembershipService.updateSwampGroupMembership($scope.selectedGroup.groupId, userId, role).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.deleteMembership = function () {
        MembershipService.deleteSwampGroupMembership($scope.selectedGroup.groupId, $scope.selectedMember.userId).then(function (response) {
            $modal.close(response);
        });
    };

    $scope.dismissModal = function (delay) {
        $modal.dismiss(delay);
    };
};

// Minification-safe dependency injection
MemberModalController.$inject = ["$scope", "$modal", "MembershipService", "UserService", "SwampConstants", "SelectedGroup", "SelectedMember", "CurrentMember", "MemberList"];
SwampControllers.controller("MemberModalController", MemberModalController);
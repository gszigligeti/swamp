/**
 * Application controller responsible for interacting with the pocket entry-related modals
 * @author Gergely Szigligeti
 */
var PocketEntryModalController = function ($scope, $modal, PocketEntryService, ParentPocket, SelectedPocketEntry) {
    $scope.entryDetails = {};
    $scope.selectedPocketEntry = SelectedPocketEntry;

    $scope.createNewPocketEntry = function () {
        if ($scope.createPocketEntryForm.$valid) {
            $scope.entryDetails.parentPocket = ParentPocket.pocketId;

            PocketEntryService.createNewSwampPocketEntry($scope.entryDetails).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.editPocketEntry = function () {
        if ($scope.editPocketEntryForm.$valid) {
            PocketEntryService.updatePocketEntry($scope.selectedPocketEntry).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.deletePocketEntry = function () {
        PocketEntryService.deletePocketEntry($scope.selectedPocketEntry.entryId).then(function (response) {
            $modal.close(response);
        });
    };
};

// Minification-safe dependency injection
PocketEntryModalController.$inject = ["$scope", "$modal", "PocketEntryService", "ParentPocket", "SelectedPocketEntry"];
SwampControllers.controller("PocketEntryModalController", PocketEntryModalController);
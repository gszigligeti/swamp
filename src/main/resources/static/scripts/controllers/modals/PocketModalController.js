/**
 * Application controller responsible for interacting with the pocket-related modals
 * @author Gergely Szigligeti
 */
var PocketModalController = function ($scope, $modal, PocketService, Parent, SelectedPocket) {
    $scope.pocketDetails = {};
    $scope.selectedPocket = SelectedPocket;

    $scope.createNewPocket = function () {
        if ($scope.createPocketForm.$valid) {
            var ownerId = Parent.userId || Parent.groupId;
            var pocketType = Parent.userId ? "USER" : "GROUP";

            PocketService.createNewSwampPocket(ownerId, pocketType, $scope.pocketDetails).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.editPocket = function () {
        if ($scope.editPocketForm.$valid) {
            PocketService.updateExistingSwampPocket($scope.selectedPocket).then(function (response) {
                $modal.close(response);
            });
        }
    };

    $scope.deletePocket = function () {
        PocketService.deleteExistingSwampPocket($scope.selectedPocket.pocketId).then(function (response) {
            $modal.close(response);
        });
    };

    $scope.dismissModal = function (delay) {
        $modal.dismiss(delay);
    };
};

// Minification-safe dependency injection
PocketModalController.$inject = ["$scope", "$modal", "PocketService", "Parent", "SelectedPocket"];
SwampControllers.controller("PocketModalController", PocketModalController);
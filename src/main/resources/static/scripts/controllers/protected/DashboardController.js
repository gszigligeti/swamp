/**
 * Application controller responsible for interacting with the dashboard page
 * @author Gergely Szigligeti
 */
var DashboardController = function ($scope, IdentityService) {

};

// Minification-safe dependency injection
DashboardController.$inject = ["$scope", "IdentityService"];
SwampControllers.controller("DashboardController", DashboardController);
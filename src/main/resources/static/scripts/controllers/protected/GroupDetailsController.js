/**
 * Application controller responsible for interacting with the group details page
 * @author Gergely Szigligeti
 */
var GroupDetailsController = function ($scope, $state, $stateParams, GroupService, MembershipService, PocketService, ModalService, IdentityService) {
    $scope.notifications = {};
    $scope.currentUser = IdentityService.getIdentity();

    GroupService.findSwampGroupById($stateParams.groupId).then(function (group) {
        $scope.group = group.data;

        MembershipService.findMembersOfSwampGroup($stateParams.groupId).then(function (members) {
            $scope.members = members.data;
        });

        PocketService.findSwampPockets($stateParams.groupId).then(function (pockets) {
            $scope.pockets = pockets.data;
        });

        $scope.editGroup = function () {
            ModalService.showModal({
                controller: "GroupModalController",
                templateUrl: "templates/modals/edit-group.html",
                resolve: {
                    SelectedGroup: angular.copy($scope.group)
                },
                onSuccess: function (result) {
                    GroupService.findSwampGroupById($stateParams.groupId).then(function (group) {
                        $scope.group = group.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.GROUP_EDITED";
                    });
                }
            });
        };

        $scope.deleteGroup = function () {
            ModalService.showModal({
                controller: "GroupModalController",
                templateUrl: "templates/modals/delete-group.html",
                resolve: {
                    SelectedGroup: angular.copy($scope.group)
                },
                onSuccess: function (result) {
                    $state.go("protected.groups", {
                        groupWasDeleted: true
                    });
                }
            });
        };

        $scope.addGroupMember = function () {
            ModalService.showModal({
                controller: "MemberModalController",
                templateUrl: "templates/modals/add-member.html",
                resolve: {
                    SelectedGroup: angular.copy($scope.group),
                    SelectedMember: null,
                    CurrentMember: angular.copy($scope.group.membership),
                    MemberList: angular.copy($scope.members)
                },
                onSuccess: function (result) {
                    MembershipService.findMembersOfSwampGroup($stateParams.groupId).then(function (members) {
                        $scope.members = members.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.MEMBER_ADDED";
                    });
                }
            });
        };

        $scope.editGroupMember = function (member) {
            ModalService.showModal({
                controller: "MemberModalController",
                templateUrl: "templates/modals/edit-member.html",
                resolve: {
                    SelectedGroup: angular.copy($scope.group),
                    SelectedMember: angular.copy(member),
                    CurrentMember: angular.copy($scope.group.membership),
                    MemberList: null
                },
                onSuccess: function (result) {
                    MembershipService.findMembersOfSwampGroup($stateParams.groupId).then(function (members) {
                        $scope.members = members.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.MEMBER_EDITED";
                    });
                }
            });
        };

        $scope.deleteGroupMember = function (member) {
            ModalService.showModal({
                controller: "MemberModalController",
                templateUrl: "templates/modals/delete-member.html",
                resolve: {
                    SelectedGroup: angular.copy($scope.group),
                    SelectedMember: angular.copy(member),
                    CurrentMember: angular.copy($scope.group.membership),
                    MemberList: null
                },
                onSuccess: function (result) {
                    MembershipService.findMembersOfSwampGroup($stateParams.groupId).then(function (members) {
                        $scope.members = members.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.MEMBER_DELETED";
                    });
                }
            });
        };

        $scope.addGroupPocket = function () {
            ModalService.showModal({
                controller: "PocketModalController",
                templateUrl: "templates/modals/add-pocket.html",
                resolve: {
                    Parent: angular.copy($scope.group),
                    SelectedPocket: null,
                },
                onSuccess: function (result) {
                    PocketService.findSwampPockets($stateParams.groupId).then(function (pockets) {
                        $scope.pockets = pockets.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.POCKET_ADDED";
                    });
                }
            });
        };

        $scope.editGroupPocket = function (pocket) {
            ModalService.showModal({
                controller: "PocketModalController",
                templateUrl: "templates/modals/edit-pocket.html",
                resolve: {
                    Parent: null,
                    SelectedPocket: angular.copy(pocket),
                },
                onSuccess: function (result) {
                    PocketService.findSwampPockets($stateParams.groupId).then(function (pockets) {
                        $scope.pockets = pockets.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.POCKET_EDITED";
                    });
                }
            });
        };

        $scope.deleteGroupPocket = function (pocket) {
            ModalService.showModal({
                controller: "PocketModalController",
                templateUrl: "templates/modals/delete-pocket.html",
                resolve: {
                    Parent: null,
                    SelectedPocket: angular.copy(pocket),
                },
                onSuccess: function (result) {
                    PocketService.findSwampPockets($stateParams.groupId).then(function (pockets) {
                        $scope.pockets = pockets.data;
                        $scope.notifications.operationSuccess = true;
                        $scope.notifications.message = "GROUP_DETAILS.POCKET_DELETED";
                    });
                }
            });
        };

        $scope.getPocketInitialClass = function (pocket) {
            var value = pocket.balance - pocket.initialValue;
            return value > 0 ? ("positive") : (value == 0 ? ("same") : ("negative"));
        };

        $scope.getBalanceTextClass = function (pocket) {
            var value = pocket.balance - pocket.initialValue;
            return value > 0 ? ("text-success") : (value == 0 ? ("text-warning") : ("text-danger"));
        };

        $scope.memberIsEditable = function (member) {
            selectedUserIsOwner = member.memberRole == "OWNER";
            selfReference = member.userId == $scope.currentUser.userId;
            userIsGroupOwner = IdentityService.hasGroupPermission($scope.group.membership, "SUPERUSER");
            userIsSystemAdmin = IdentityService.hasRole("ADMIN");
            userIsAdministrator = IdentityService.hasGroupPermission($scope.group.membership, "PRIVILEGED");
            selectedUserIsAdmin = member.memberRole == "ADMIN";

            return !selfReference && !selectedUserIsOwner && (userIsSystemAdmin || userIsGroupOwner || (userIsAdministrator && !selectedUserIsAdmin));
        };
    }).catch(function (response) {
        $scope.unauthorized = (response.status == 401 || response.status == 403);
    });
};

// Minification-safe dependency injection
GroupDetailsController.$inject = ["$scope", "$state", "$stateParams", "GroupService", "MembershipService", "PocketService", "ModalService", "IdentityService"];
SwampControllers.controller("GroupDetailsController", GroupDetailsController);
/**
 * Application controller responsible for interacting with the group list page
 * @author Gergely Szigligeti
 */
var GroupListController = function ($scope, $state, IdentityService, GroupService, ModalService) {
    $scope.currentUser = IdentityService.getIdentity();
    $scope.notifications = {};

    GroupService.findEverySwampGroup().then(function (groupList) {
        $scope.groupList = groupList.data;
    });

    if ($state.params.groupWasDeleted) {
        $scope.notifications.groupDeleteSuccess = true;
    }

    $scope.addNewGroup = function () {
        ModalService.showModal({
            controller: "GroupModalController",
            templateUrl: "templates/modals/add-group.html",
            resolve: {
                SelectedGroup: null
            },
            onSuccess: function (result) {
                GroupService.findEverySwampGroup().then(function (groupList) {
                    $scope.notifications.groupAddSuccess = true;
                    $scope.groupList = groupList.data;
                });
            }
        });
    };

    $scope.filterFunction = function (element) {
        return $scope.onlyMyGroups ? (element.members.indexOf($scope.currentUser.userId) !== -1) : true;
    };
};

// Minification-safe dependency injection
GroupListController.$inject = ["$scope", "$state", "IdentityService", "GroupService", "ModalService"];
SwampControllers.controller("GroupListController", GroupListController);
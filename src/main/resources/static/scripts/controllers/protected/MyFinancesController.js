/**
 * Application controller responsible for interacting with the my finances page
 * @author Gergely Szigligeti
 */
var MyFinancesController = function ($scope, ModalService, PocketService, IdentityService) {
    $scope.chartData = [];
    $scope.chartOptions = {};
    $scope.notifications = {};
    $scope.currentUser = IdentityService.getIdentity();

    PocketService.findSwampPockets($scope.currentUser.userId).then(function (pockets) {
        $scope.pockets = pockets.data;
    });

    $scope.addUserPocket = function () {
        ModalService.showModal({
            controller: "PocketModalController",
            templateUrl: "templates/modals/add-pocket.html",
            resolve: {
                Parent: $scope.currentUser,
                SelectedPocket: null,
            },
            onSuccess: function (result) {
                PocketService.findSwampPockets($scope.currentUser.userId).then(function (pockets) {
                    $scope.pockets = pockets.data;
                    $scope.notifications.operationSuccess = true;
                    $scope.notifications.message = "GROUP_DETAILS.POCKET_ADDED";
                });
            }
        });
    };

    $scope.editUserPocket = function (pocket) {
        ModalService.showModal({
            controller: "PocketModalController",
            templateUrl: "templates/modals/edit-pocket.html",
            resolve: {
                Parent: null,
                SelectedPocket: angular.copy(pocket),
            },
            onSuccess: function (result) {
                PocketService.findSwampPockets($scope.currentUser.userId).then(function (pockets) {
                    $scope.pockets = pockets.data;
                    $scope.notifications.operationSuccess = true;
                    $scope.notifications.message = "GROUP_DETAILS.POCKET_EDITED";
                });
            }
        });
    };

    $scope.deleteUserPocket = function (pocket) {
        ModalService.showModal({
            controller: "PocketModalController",
            templateUrl: "templates/modals/delete-pocket.html",
            resolve: {
                Parent: null,
                SelectedPocket: angular.copy(pocket),
            },
            onSuccess: function (result) {
                PocketService.findSwampPockets($scope.currentUser.userId).then(function (pockets) {
                    $scope.pockets = pockets.data;
                    $scope.notifications.operationSuccess = true;
                    $scope.notifications.message = "GROUP_DETAILS.POCKET_DELETED";
                });
            }
        });
    };

    $scope.getPocketInitialClass = function (pocket) {
        var value = pocket.balance - pocket.initialValue;
        return value > 0 ? ("positive") : (value == 0 ? ("same") : ("negative"));
    };

    $scope.getBalanceTextClass = function (pocket) {
        var value = pocket.balance - pocket.initialValue;
        return value > 0 ? ("text-success") : (value == 0 ? ("text-warning") : ("text-danger"));
    };
};

// Minification-safe dependency injection
MyFinancesController.$inject = ["$scope", "ModalService", "PocketService", "IdentityService"];
SwampControllers.controller("MyFinancesController", MyFinancesController);
/**
 * Application controller responsible for interacting with the navigation bar
 * @author Gergely Szigligeti
 */
var NavigationController = function ($scope, $state, AuthenticationService, IdentityService) {
    $scope.currentUser = IdentityService.getIdentity();

    // Method for signing out the currently authenticated user
    $scope.logout = function () {
        AuthenticationService.logout().then(function (response) {
            $state.go("public.home");
        });
    };
};

// Minification-safe dependency injection
NavigationController.$inject = ["$scope", "$state", "AuthenticationService", "IdentityService"];
SwampControllers.controller("NavigationController", NavigationController);
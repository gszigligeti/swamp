/**
 * Application controller responsible for interacting with the pocket details page
 * @author Gergely Szigligeti
 */
var PocketDetailsController = function ($scope, $stateParams, PocketService, PocketEntryService, ModalService, IdentityService) {
    $scope.notifications = {};
    $scope.currentUser = IdentityService.getIdentity();

    PocketService.findSwampPocketById($stateParams.pocketId).then(function (pocketResponse) {
        $scope.pocket = pocketResponse.data;

        PocketEntryService.findSwampPocketEntries($stateParams.pocketId).then(function (entryResponse) {
            $scope.entries = entryResponse.data;
        });

        $scope.addPocketEntry = function () {
            ModalService.showModal({
                controller: "PocketEntryModalController",
                templateUrl: "templates/modals/add-pocket-entry.html",
                resolve: {
                    ParentPocket: angular.copy($scope.pocket),
                    SelectedPocketEntry: null
                },
                onSuccess: function (result) {
                    PocketService.findSwampPocketById($stateParams.pocketId).then(function (pocketResponse) {
                        $scope.pocket = pocketResponse.data;

                        PocketEntryService.findSwampPocketEntries($stateParams.pocketId).then(function (entryResponse) {
                            $scope.entries = entryResponse.data;
                            $scope.notifications.operationSuccess = true;
                            $scope.notifications.message = "POCKET_DETAILS.POCKET_ENTRY_ADDED";
                        });
                    });
                }
            });
        };

        $scope.editPocketEntry = function (pocketEntry) {
            ModalService.showModal({
                controller: "PocketEntryModalController",
                templateUrl: "templates/modals/edit-pocket-entry.html",
                resolve: {
                    ParentPocket: angular.copy($scope.pocket),
                    SelectedPocketEntry: angular.copy(pocketEntry)
                },
                onSuccess: function (result) {
                    PocketService.findSwampPocketById($stateParams.pocketId).then(function (pocketResponse) {
                        $scope.pocket = pocketResponse.data;

                        PocketEntryService.findSwampPocketEntries($stateParams.pocketId).then(function (entryResponse) {
                            $scope.entries = entryResponse.data;
                            $scope.notifications.operationSuccess = true;
                            $scope.notifications.message = "POCKET_DETAILS.POCKET_ENTRY_EDITED";
                        });
                    });
                }
            });
        };

        $scope.deletePocketEntry = function (pocketEntry) {
            ModalService.showModal({
                controller: "PocketEntryModalController",
                templateUrl: "templates/modals/delete-pocket-entry.html",
                resolve: {
                    ParentPocket: angular.copy($scope.pocket),
                    SelectedPocketEntry: angular.copy(pocketEntry)
                },
                onSuccess: function (result) {
                    PocketService.findSwampPocketById($stateParams.pocketId).then(function (pocketResponse) {
                        $scope.pocket = pocketResponse.data;

                        PocketEntryService.findSwampPocketEntries($stateParams.pocketId).then(function (entryResponse) {
                            $scope.entries = entryResponse.data;
                            $scope.notifications.operationSuccess = true;
                            $scope.notifications.message = "POCKET_DETAILS.POCKET_ENTRY_DELETED";
                        });
                    });
                }
            });
        };

        $scope.getPocketInitialClass = function () {
            var value = $scope.pocket.balance - $scope.pocket.initialValue;
            return value > 0 ? ("positive") : (value == 0 ? ("same") : ("negative"));
        };
    }).catch(function (response) {
        $scope.unauthorized = (response.status == 401 || response.status == 403);
    });
};

// Minification-safe dependency injection
PocketDetailsController.$inject = ["$scope", "$stateParams", "PocketService", "PocketEntryService", "ModalService", "IdentityService"];
SwampControllers.controller("PocketDetailsController", PocketDetailsController);
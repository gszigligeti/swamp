/**
 * Application controller responsible for interacting with the settings page
 * @author Gergely Szigligeti
 */
var SettingsPageController = function ($scope, $state, $translate, IdentityService, ModalService, AuthenticationService, UserService) {
    $scope.currentUser = IdentityService.getIdentity();
    $scope.notifications = {};
    $scope.profileDetails = angular.copy($scope.currentUser);
    $scope.accountSettings = angular.copy($scope.currentUser);
    $scope.securitySettings = angular.copy($scope.currentUser);

    $scope.passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;

    if ($state.params.updateSuccess) {
        $scope.notifications.accountUpdateSuccess = true;
    }

    $scope.updateProfileDetails = function () {
        if ($scope.profileForm.$valid) {
            UserService.updateSwampUserAccount({
                userId: $scope.profileDetails.userId,
                firstName: $scope.profileDetails.firstName,
                lastName: $scope.profileDetails.lastName,
                emailAddress: $scope.profileDetails.emailAddress
            }).then(function (response) {
                $state.transitionTo($state.current, {updateSuccess: true}, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            });
        }
    };

    $scope.updateAccountSettings = function () {
        if ($scope.accountForm.$valid) {
            UserService.updateSwampUserAccount({
                userId: $scope.accountSettings.userId,
                language: $scope.accountSettings.language
            }).then(function (response) {
                $translate.use(response.data.language);
                $state.transitionTo($state.current, {updateSuccess: true}, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            });
        }
    };

    $scope.updateSecuritySettings = function () {
        if ($scope.securityForm.$valid || ($scope.securityForm.$error.match && $scope.securitySettings.password === undefined)) {
            UserService.updateSwampUserAccount({
                userId: $scope.securitySettings.userId,
                public: $scope.securitySettings.public,
                newPassword: $scope.securitySettings.newPassword,
                currentPassword: $scope.securitySettings.currentPassword
            }).then(function (response) {
                $state.transitionTo($state.current, {updateSuccess: true}, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            });
        }
    };

    $scope.deleteAccount = function () {
        ModalService.showModal({
            controller: ["$scope", "$modal", "IdentityService", "UserService", function (scope, $modal, IdentityService, UserService) {
                scope.notifications = {};

                scope.deleteAccount = function () {
                    UserService.deleteSwampUserAccount(IdentityService.getIdentity().userId, scope.passwordConfirmation).then(function (response) {
                        $modal.close(response);
                    }, function (response) {
                        if (response.data.errorMessage == "PASSWORD_MATCH_ERROR") {
                            scope.notifications.passwordMatchError = true;
                        }
                    });
                };
            }],
            templateUrl: "templates/modals/delete-account.html",
            onSuccess: function (result) {
                if (result.status == 204) {
                    AuthenticationService.logout().then(function () {
                        $state.go("public.home");
                    });
                }
            }
        });
    };
};

// Minification-safe dependency injection
SettingsPageController.$inject = ["$scope", "$state", "$translate", "IdentityService", "ModalService", "AuthenticationService", "UserService"];
SwampControllers.controller("SettingsPageController", SettingsPageController);
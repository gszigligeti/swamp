/**
 * Application controller responsible for activating user accounts
 * @author Gergely Szigligeti
 */
var ActivationController = function ($scope, $state, $stateParams, AuthenticationService) {
    // Immediately call the activation service, when the controller is instantiated
    AuthenticationService.activateAccount($stateParams.code).then(function (response) {
        $state.go("public.login", {
            accountActivation: true
        });
    }, function (response) {
        $state.go("public.login", {
            accountActivation: false
        });
    });
};

// Minification-safe dependency injection
ActivationController.$inject = ["$scope", "$state", "$stateParams", "AuthenticationService"];
SwampControllers.controller("ActivationController", ActivationController);
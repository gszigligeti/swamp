/**
 * Application controller responsible for interacting with the home page
 * @author Gergely Szigligeti
 */
var HomeController = function ($scope, $translate) {
    $scope.changeSiteLanguage = function (languageCode) {
        $translate.use(languageCode);
    };
};

// Minification-safe dependency injection
HomeController.$inject = ["$scope", "$translate"];
SwampControllers.controller("HomeController", HomeController);
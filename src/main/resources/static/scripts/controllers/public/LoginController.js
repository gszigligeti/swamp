/**
 * Application controller responsible for interacting with the login page
 * @author Gergely Szigligeti
 */
var LoginController = function ($scope, $state, $translate, AuthenticationService) {
    $scope.credentials = {};
    $scope.notifications = {};
    $scope.passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;

    $scope.notifications.accountActivationSuccess = ($state.params.accountActivation === true);
    $scope.notifications.accountActivationFailure = ($state.params.accountActivation === false);

    // Method for signing in the user with the given credentials
    $scope.authenticateUser = function () {
        if ($scope.loginForm.$valid) {
            AuthenticationService.login($scope.credentials).then(function (response) {
                $scope.notifications.authenticationFailure = false;
                $translate.use(response.identity.language);
                $state.go("protected.dashboard");
            }, function (response) {
                $scope.notifications.authenticationFailure = true;
                $scope.authenticationErrorMessage = "ERROR_MESSAGES." + response.data.error_description;
            });
        }
    };

    $scope.getClassForField = function (field) {
        if (field.$dirty && field.$invalid) {
            return "has-error";
        }

        if (field.$dirty && field.$valid) {
            return "has-success";
        }
    };
};

// Minification-safe dependency injection
LoginController.$inject = ["$scope", "$state", "$translate", "AuthenticationService"];
SwampControllers.controller("LoginController", LoginController);
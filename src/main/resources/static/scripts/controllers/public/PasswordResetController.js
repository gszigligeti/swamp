/**
 * Application controller responsible for interacting with the password reset page
 * @author Gergely Szigligeti
 */
var PasswordResetController = function ($scope, $stateParams, AuthenticationService) {
    $scope.tokenProvided = $stateParams.token != undefined;
    $scope.notifications = {};
    $scope.passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;

    // Method for requesting a password reset email
    $scope.requestPasswordResetEmail = function (emailAddress) {
        AuthenticationService.requestPasswordResetEmail(emailAddress).then(function (response) {
            $scope.notifications.emailSendingSuccess = true;
            $scope.notifications.emailSendingFailure = false;
        }, function (response) {
            $scope.notifications.emailSendingSuccess = false;
            $scope.notifications.emailSendingFailure = true;
            $scope.errorMessage = "ERROR_MESSAGES." + response.data.errorMessage;
        });
    };

    // Method for updating the password authorized by the token
    $scope.resetForgottenPassword = function (password) {
        AuthenticationService.resetForgottenPassword($stateParams.token, password).then(function (response) {
            $scope.notifications.passwordUpdateSuccess = true;
            $scope.notifications.passwordUpdateFailure = false;
        }, function (response) {
            $scope.notifications.passwordUpdateSuccess = false;
            $scope.notifications.passwordUpdateFailure = true;
            $scope.errorMessage = "ERROR_MESSAGES." + response.data.errorMessage;
        });
    };

    $scope.getClassForField = function (field) {
        if (field.$dirty && field.$invalid) {
            return "has-error";
        }

        if (field.$dirty && field.$valid) {
            return "has-success";
        }
    };
};

// Minification-safe dependency injection
PasswordResetController.$inject = ["$scope", "$stateParams", "AuthenticationService"];
SwampControllers.controller("PasswordResetController", PasswordResetController);
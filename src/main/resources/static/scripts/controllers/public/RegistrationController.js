/**
 * Application controller responsible for interacting with the registration page
 * @author Gergely Szigligeti
 */
var RegistrationController = function ($scope, $translate, AuthenticationService) {
    $scope.notifications = {};
    $scope.passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{8,}$/;

    // Method for registering a new account with the given details
    $scope.registerSwampUser = function () {
        if ($scope.registrationForm.$valid) {
            $scope.accountDetails.language = $translate.use();
            AuthenticationService.register($scope.accountDetails).then(function (response) {
                $scope.notifications.registrationSuccess = true;
                $scope.notifications.error = false;
            }, function (response) {
                $scope.notifications.error = true;
                $scope.errorMessage = "ERROR_MESSAGES." + response.data.errorMessage;
                $scope.notifications.registrationSuccess = false;
            });
        }
    };

    $scope.getClassForField = function (field) {
        if (field.$dirty && field.$invalid) {
            return "has-error";
        }

        if (field.$dirty && field.$valid) {
            return "has-success";
        }
    };
};

// Minification-safe dependency injection
RegistrationController.$inject = ["$scope", "$translate", "AuthenticationService"];
SwampControllers.controller("RegistrationController", RegistrationController);
/**
 * Application directive for hiding and showing DOM elements based on authorities
 * @author Gergely Szigligeti
 */
var AuthorityDirective = function (IdentityService) {
    return {
        restrict: "A",
        scope: {
            hasGroupPermission: "@",
            hasPocketPermission: "@",
            membership: "=",
            pocket: "=",
            hasGroupRole: "@",
            hasRole: "@"
        },
        link: function ($scope, $element, $attributes) {
            var isSystemAdmin = IdentityService.hasRole("ADMIN");

            // If we search for group permission
            if (angular.isDefined($scope.hasGroupPermission)) {
                $scope.$watch("membership", function (membership) {
                    if (!isSystemAdmin && (membership == null || !IdentityService.hasGroupPermission(membership, $scope.hasGroupPermission))) {
                        $element.remove();
                    }
                });
            }

            if (angular.isDefined($scope.hasPocketPermission)) {
                $scope.$watch("pocket", function (pocket) {
                    var membership = pocket.ownerGroup.membership;
                    if (pocket.ownerGroup != null && !isSystemAdmin && (membership == null || !IdentityService.hasGroupPermission(membership, $scope.hasGroupPermission))) {
                        $element.remove();
                    }
                });
            }

            // If we search for group role
            if (angular.isDefined($scope.hasGroupRole)) {
                $scope.$watch("membership", function (membership) {
                    if (!IdentityService.hasGroupRole($scope.hasGroupRole)) {
                        $element.remove();
                    }
                });
            }

            // If we search for user role
            if (angular.isDefined($scope.hasRole)) {
                if (!IdentityService.hasRole($scope.hasRole)) {
                    $element.remove();
                }
            }
        }
    };
};

// Minification-safe dependency injection
AuthorityDirective.$inject = ["IdentityService"];
SwampDirectives.directive("authorized", AuthorityDirective);
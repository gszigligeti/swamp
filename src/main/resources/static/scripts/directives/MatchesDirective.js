/**
 * Application directive for ensuring the identity of two models
 * @author Gergely Szigligeti
 */
var MatchesDirective = function () {
    return {
        require: "ngModel",
        scope: {
            ngMatch: "=ngMatch"
        },
        link: function($scope, element, attributes, $modelController) {
            $modelController.$validators.match = function (modelValue) {
                return modelValue !== undefined && $scope.ngMatch !== undefined && modelValue == $scope.ngMatch;
            };
 
            $scope.$watch("ngMatch", function() {
                $modelController.$validate();
            });
        }
    };
};

// Minification-safe dependency injection
MatchesDirective.$inject = [];
SwampDirectives.directive("ngMatch", MatchesDirective);
/**
 * Application directive for displaying and hiding notifications based on expression value
 * @author Gergely Szigligeti
 */
var NotificationDirective = function ($timeout) {
    return {
        replace: true,
        require: "ngModel",
        restrict: "E",
        scope: {
            delay: "@",
            duration: "@",
            ngModel: "=",
            text: "@",
            type: "@"
        },
        template: "<div class='alert' ng-class='getAlertType()' role='alert'><i ng-class='getIconClass()'></i>&ensp;{{text | translate}}</div>",
        link: function ($scope, $element, $attributes, $modelController) {
            var delay = $scope.delay || 2000;
            var duration = $scope.duration || 500;
            var type = $scope.type || "info";

            $element.hide();

            $scope.$watch("ngModel", function (modelValue) {
                if (modelValue === true) {
                    $element.slideDown(duration);

                    $timeout(function () {
                        $element.slideUp(duration);
                        $modelController.$setViewValue(false);
                        $modelController.$render();
                    }, delay);
                }
            });

            $scope.getIconClass = function () {
                switch (type) {
                    case "success":
                        return "fa fa-check";
                    case "info":
                        return "fa fa-info";
                    case "warning":
                        return "fa fa-exclamation";
                    case "danger":
                        return "fa fa-times";
                }
            };

            $scope.getAlertType = function () {
                return "alert-" + type;
            };
        }
    };
};

// Minification-safe dependency injection
NotificationDirective.$inject = ["$timeout"];
SwampDirectives.directive("notification", NotificationDirective);
/**
 * Application directive for scroll-aware elements. This directive marks the
 * HTML element with a custom CSS class if the scrolling limit is reached
 * @author Gergely Szigligeti
 */
var ScrollAwareDirective = function ($window) {
    return {
        restrict: "A",
        scope: false,
        link: function ($scope, $element, $attributes) {
            // Watch for scroll event and apply special class on element if limit is reached
            angular.element($window).bind("scroll", function () {
                var scrollLimitNotReached = this.scrollY < Number($attributes.scrollLimit);
                return scrollLimitNotReached ? $element.removeClass("scroll-aware") : $element.addClass("scroll-aware");
            });
        }
    };
};

// Minification-safe dependency injection
ScrollAwareDirective.$inject = ["$window"];
SwampDirectives.directive("scrollAware", ScrollAwareDirective);
/**
 * Application directive for scrolling with a nice smooth moving effect
 * @author Gergely Szigligeti
 */
var SmoothScrollDirective = function ($window, EasingFunctions) {
    return {
        restrict: "A",
        scope: false,
        link: function ($scope, $element, $attributes) {
            $element.bind("click", function (event) {
                // Basic configuration (everything in ms)
                var elapsedTime = 0;
                var animationDuration = $attributes.duration || 900;
                var updateInterval = 10;

                var startPosition = $window.scrollY;
                var targetPosition = angular.element(document.querySelector($attributes.smoothScroll)).prop("offsetTop") - 60;
                var distance = Math.abs(targetPosition - startPosition);
                var multiplier = targetPosition > startPosition ? 1 : -1;
                var easingType = $attributes.easingType || "easeInOutQuart";

                var scrollAnimation = function () {
                    timePercentage = (elapsedTime / animationDuration);
                    position = startPosition + (distance * EasingFunctions[easingType](timePercentage > 1 ? 1 : timePercentage)) * multiplier;

                    $window.scrollTo(0, position);

                    elapsedTime += updateInterval;

                    // We watch for the variable not the real scroll position, so we don't
                    // get an infinite loop if the element is unreachable
                    if (position == targetPosition) {
                        clearInterval(runScrollAnimation);
                    }
                };

                // Loop the animation function in the interval that has been configured
                var runScrollAnimation = setInterval(scrollAnimation, updateInterval);
            });
        }
    };
};

// Minification-safe dependency injection
SmoothScrollDirective.$inject = ["$window", "EasingFunctions"];
SwampDirectives.directive("smoothScroll", SmoothScrollDirective);
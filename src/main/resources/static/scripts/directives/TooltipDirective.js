/**
 * Application directive for displaying a tooltip beside the given element
 * @author Gergely Szigligeti
 */
var TooltipDirective = function () {
    return {
        restrict: "A",
        scope: {
            text: "@",
            placement: "@"
        },
        link: function ($scope, $element, $attributes) {
            $scope.$watch("text", function (textValue) {
                $element.tooltip({
                    title: $scope.text,
                    placement: $scope.placement || "top"
                });
            });
        }
    };
};

// Minification-safe dependency injection
TooltipDirective.$inject = [];
SwampDirectives.directive("tooltip", TooltipDirective);
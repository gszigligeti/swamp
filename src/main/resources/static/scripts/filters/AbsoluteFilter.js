/**
 * Application filter for displaying a number's absolute value
 * @author Gergely Szigligeti
 */
var AbsoluteFilter = function () {
    return function (value) {
        return Math.abs(value);
    };
};

// Minification-safe dependency injection
AbsoluteFilter.$inject = [];
SwampFilters.filter("abs", AbsoluteFilter);
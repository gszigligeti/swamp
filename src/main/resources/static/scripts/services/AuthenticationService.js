/**
 * Application service responsible for authenticating and creating user accounts
 * @author Gergely Szigligeti
 */
var AuthenticationService = function ($q, $http, $httpParamSerializer, TokenService, IdentityService, SwampConstants) {
    // Method for authenticating a user by email/password combination
    this.login = function (credentials) {
        var requestData = new Object();
        var deferredResponse = $q.defer();

        requestData.client_id = SwampConstants.oauth.clientId;
        requestData.username = credentials.emailAddress;
        requestData.password = credentials.password;
        requestData.grant_type = SwampConstants.oauth.passwordGrantType;

        // After the AJAX request was successfully made, we store the token credentials
        $http.post("/api/authentication/token", $httpParamSerializer(requestData), {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).then(function (response) {
            TokenService.saveCredentials(response.data.access_token, response.data.refresh_token, response.data.expires_in);
            IdentityService.resolveIdentity().then(function (identityInfo) {
                deferredResponse.resolve({response: response.data, identity: identityInfo.identity});
            });
        }, function (response) {
            deferredResponse.reject(response);
        });

        return deferredResponse.promise;
    };

    // Method for registering a new user account
    this.register = function (accountDetails) {
        return $http.post("/api/authentication/register", accountDetails);
    };

    // Method for activating a freshly created user account
    this.activateAccount = function (activationCode) {
        return $http.post("/api/authentication/activate?code=" + activationCode);
    };

    // Method for requesting an email with the password reset link
    this.requestPasswordResetEmail = function (emailAddress) {
        return $http.get("/api/authentication/reset?email=" + emailAddress);
    };

    // Method for updating the forgotten password to a new one
    this.resetForgottenPassword = function (token, password) {
        return $http.post("/api/authentication/reset?token=" + token + "&password=" + password);
    };

    // Method for logging out the authenticated user
    this.logout = function () {
        return $http.post("/api/authentication/logout").success(function () {
            TokenService.removeCredentials();
            IdentityService.forgetIdentity();
        });
    };
};

// Minification-safe dependency injection
AuthenticationService.$inject = ["$q", "$http", "$httpParamSerializer", "TokenService", "IdentityService", "SwampConstants"];
SwampServices.service("AuthenticationService", AuthenticationService);
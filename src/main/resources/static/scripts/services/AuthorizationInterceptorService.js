/**
 * HTTP interceptor service for adding Authorization header and refreshing token upon expiration
 * @author Gergely Szigligeti
 */
var AuthorizationInterceptorService = function ($q, $injector) {
    // Method for intercepting HTTP requests
    this.request = function (httpConfiguration) {
        var TokenService = $injector.get("TokenService");

        if (!TokenService.isAccessTokenExpired()) {
            httpConfiguration.headers.Authorization = "Bearer " + TokenService.getAccessToken();
        }

        return httpConfiguration;
    };

    // Method for intercepting HTTP response errors (possible cause can be token expiration)
    this.responseError = function (response) {
        var TokenService = $injector.get("TokenService");
        var IdentityService = $injector.get("IdentityService");

        if (response.status == 401 && response.data.error == "invalid_token") {
            TokenService.refreshAccessToken().then(function (tokenResponse) {
                $q.resolve(tokenResponse);
            });
        }

        return $q.reject(response);
    };
};

// Minification-safe dependency injection
AuthorizationInterceptorService.$inject = ["$q", "$injector"];
SwampServices.service("AuthorizationInterceptorService", AuthorizationInterceptorService);
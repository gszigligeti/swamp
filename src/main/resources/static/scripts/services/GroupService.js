/**
 * Application service responsible for handling group-related operations
 * @author Gergely Szigligeti
 */
var GroupService = function ($q, $http) {
    // Method for creating a new group in the system
    this.createSwampGroup = function (groupOverview) {
        return $http.post("/api/groups", groupOverview);
    };

    // Method for fetching every group from the database
    this.findEverySwampGroup = function () {
        return $http.get("/api/groups");
    };

    // Method for finding an existing group by it's identifier hash
    this.findSwampGroupById = function (groupId) {
        return $http.get("/api/groups/" + groupId);
    };

    // Method for updating an existing group with new details
    this.updateSwampGroup = function (groupOverview) {
        return $http.put("/api/groups/" + groupOverview.groupId, groupOverview);
    };

    // Method for removing an existing group by it's identifier hash
    this.deleteSwampGroupById = function (groupId) {
        return $http.delete("/api/groups/" + groupId);
    };
};

// Minification-safe dependency injection
GroupService.$inject = ["$q", "$http"];
SwampServices.service("GroupService", GroupService);
/**
 * Application service responsible for storing the identity of the user
 * @author Gergely Szigligeti
 */
var IdentityService = function ($q, $http, SwampConstants) {
    this.authenticated = false, this.identity = undefined, self = this;

    // Method for checking if the user is authenticated
    self.isAuthenticated = function() {
        return self.authenticated;
    };

    // Method for checking if the identity has been resolved
    self.isIdentityResolved = function() {
        return angular.isDefined(self.identity);
    };

    // Method for calling backend in order to retrieve identity
    self.resolveIdentity = function() {
        var deferredResponse = $q.defer();

        $http.get("/api/authentication/me").then(function (response) {
            self.authenticated = true;
            self.identity = response.data;
            deferredResponse.resolve({authenticated: self.authenticated, identity: self.identity});
        }, function (response) {
            self.authenticated = false;
            self.identity = undefined;
            deferredResponse.resolve({authenticated: self.authenticated, identity: self.identity});
        });

        return deferredResponse.promise;
    };

    // Method for determining whether the user has a particular role
    self.hasRole = function (roleName) {
        return self.isIdentityResolved() && self.identity.roles.some(function (role) {
            return role === roleName;
        });
    };

    // Method for determining whether the user has a particular role in a group
    self.hasGroupRole = function (membership, roleName) {
        return self.isIdentityResolved() && membership.memberRole == roleName;
    };

    // Method for determining whether the user has a particular permission in a group
    self.hasGroupPermission = function (membership, permission) {
        return self.isIdentityResolved() && membership && SwampConstants.groupPermissions[permission].indexOf(membership.memberRole) !== -1;
    };

    // Method for accessing the current user's identity
    self.getIdentity = function() {
        return self.identity;
    };

    // Method for deleting the identity
    self.forgetIdentity = function () {
        self.authenticated = false;
        self.identity = undefined;
    };
};

// Minification-safe dependency injection
IdentityService.$inject = ["$q", "$http", "SwampConstants"];
SwampServices.service("IdentityService", IdentityService);
/**
 * Application service responsible for handling membership-related operations
 * @author Gergely Szigligeti
 */
var MembershipService = function ($q, $http) {
    // Method for creating a new membership relation between the given group and user
    this.addSwampGroupMembership = function (groupId, userId, role) {
        return $http.post("/api/groups/" + groupId + "/members?userId=" + userId + "&role=" + role);
    };

    // Method for fetching every member of a given group
    this.findMembersOfSwampGroup = function (groupId) {
        return $http.get("/api/groups/" + groupId + "/members");
    };

    // Method for updating an existing membership
    this.updateSwampGroupMembership = function (groupId, userId, role) {
        return $http.put("/api/groups/" + groupId + "/members?userId=" + userId + "&role=" + role);
    };

    // Method for deleting a member from a given group
    this.deleteSwampGroupMembership = function (groupId, userId) {
        return $http.delete("/api/groups/" + groupId + "/members?userId=" + userId);
    };
};

// Minification-safe dependency injection
MembershipService.$inject = ["$q", "$http"];
SwampServices.service("MembershipService", MembershipService);
/**
 * Application service responsible for handling modal events in a nice and manageable way
 * Based on dwmkerr's angular-modal-service (https://github.com/dwmkerr/angular-modal-service)
 * @author Gergely Szigligeti
 */
var ModalService = function ($animate, $document, $compile, $controller, $rootScope, $q, $log, $templateRequest, $timeout) {
    var documentBody = $document.find("body"), self = this;

    // Method for resolving the modal template
    this.getTemplate = function (template, templateUrl) {
        var deferred = $q.defer();

        // If neither a template nor a templateUrl is defined, then reject the promise
        if (!angular.isDefined(template) && !angular.isDefined(templateUrl)) {
            $log.error("No template or templateUrl defined");
            deferred.reject("No template or templateUrl defined");
        }

        // If we have a ready-to-use template, then resolve it
        if (angular.isDefined(template)) {
            deferred.resolve(template);
        }

        // If we have a url that needs to be fetched, then make the request and resolve the promise
        if (angular.isDefined(templateUrl)) {
            $templateRequest(templateUrl, true).then(function (template) {
                deferred.resolve(template);
            }).catch(function (error) {
                $log.error(error);
                deferred.reject(error);
            });
        }

        return deferred.promise;
    };

    // Method for preparing the modal element
    this.showModal = function (options) {
        var deferred = $q.defer();

        // We always need a controller for the modal
        if (!angular.isDefined(options.controller)) {
            $log.error("No controller has been specified");
            deferred.reject("No controller has been specified");
            return deferred.promise;
        }

        // Fetch the template from the given parameters
        self.getTemplate(options.template, options.templateUrl).then(function (template) {
            // Create the modal scope, initialize the DOM element and define callback functions
            var modalScope = (options.scope || $rootScope).$new();
            var templateLinkFunction = $compile(template);
            var modalElement = templateLinkFunction(modalScope);

            // Variables that need to be injected into the controller
            var resolve = {
                $scope : modalScope,
                $modal: {
                    dismiss: function (delay) {
                        delay = delay || 150;
                        modalElement.modal("hide");
                        
                        $timeout(function () {
                            $animate.leave(modalElement).then(function () {
                                angular.element($document[0].querySelector(".modal-backdrop")).remove();
                                modalScope.$destroy();
                            });
                        }, delay);
                    },
                    close: function (result, delay) {
                        delay = delay || 150;
                        modalElement.modal("hide");

                        $timeout(function () {
                            $animate.leave(modalElement).then(function () {
                                (options.onSuccess || angular.noop)(result);
                                angular.element($document[0].querySelector(".modal-backdrop")).remove();
                                modalScope.$destroy();
                            });
                        }, delay);
                    }
                }
            };

            // If the user has defined custom variables, then add them to the list
            if (options.resolve) {
                angular.extend(resolve, options.resolve);
            }

            // Create the controller for the modal
            var modalController = $controller(options.controller, resolve, false, options.controllerAs);

            if (options.controllerAs && modalScope[options.controllerAs]) {
                angular.extend(modalController, modalScope[options.controllerAs]);
            }

            // Append the created element to the body or custom element
            self.appendElement(angular.isDefined(options.appendTo) ? options.appendTo : documentBody, modalElement);

            // Create and show the modal
            modalElement.modal("show");

            // Resolve the promise and provide the callbacks
            deferred.resolve();
        }).catch(function (error) {
            $log.error(error);
            deferred.reject(error);
        });

        return deferred.promise;
    };

    this.appendElement = function (parentElement, element) {
        var childElements = parentElement.children();
        return $animate.enter(element, parentElement, childElements.length > 0 ? childElements[childElements.length -1] : angular.noop);
    };
};

// Minification-safe dependency injection
ModalService.$inject = ["$animate", "$document", "$compile", "$controller", "$rootScope", "$q", "$log", "$templateRequest", "$timeout"];
SwampServices.service("ModalService", ModalService);
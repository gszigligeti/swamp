/**
 * Application service responsible for handling pocket entry-related operations
 * @author Gergely Szigligeti
 */
var PocketEntryService = function ($q, $http) {
    // Method for creating a new pocket entry in a given pocket
    this.createNewSwampPocketEntry = function (entryDetails) {
        return $http.post("/api/entries", entryDetails);
    };

    // Method for fetching every entry or entries within a given pocket (if specified)
    this.findSwampPocketEntries = function (ownerId) {
        return $http.get("/api/entries" + (ownerId ? "?ownerId=" + ownerId : ""));
    };

    // Method for fetching a pocket entry by it's identifier
    this.findPocketEntryById = function (entryId) {
        return $http.get("/api/entries/" + entryId);
    };

    // Method for updating an existing pocket entry with new details
    this.updatePocketEntry = function (entryDetails) {
        return $http.put("/api/entries/" + entryDetails.entryId, entryDetails);
    };

    // Method for removing a entry from a given pocket
    this.deletePocketEntry = function (entryId) {
        return $http.delete("/api/entries/" + entryId);
    };
};

// Minification-safe dependency injection
PocketEntryService.$inject = ["$q", "$http"];
SwampServices.service("PocketEntryService", PocketEntryService);
/**
 * Application service responsible for handling pocket-related operations
 * @author Gergely Szigligeti
 */
var PocketService = function ($q, $http) {
    // Method for creating a new pocket and assigning it to it's owner
    this.createNewSwampPocket = function (ownerId, pocketType, pocketDetails) {
        return $http.post("/api/pockets?ownerId=" + ownerId + "&pocketType=" + pocketType, pocketDetails);
    };

    // Method for getting every pocket or pockets assigned to a given owner (if specified)
    this.findSwampPockets = function (ownerId) {
        return $http.get("/api/pockets" + (ownerId ? "?ownerId=" + ownerId : ""));
    };

    // Method for fetching a pocket by it's identifier
    this.findSwampPocketById = function (pocketId) {
        return $http.get("/api/pockets/" + pocketId);
    };

    // Method for updating a pocket with the provided new details
    this.updateExistingSwampPocket = function (pocketDetails) {
        return $http.put("/api/pockets/" + pocketDetails.pocketId, pocketDetails);
    };

    // Method for removing a given pocket
    this.deleteExistingSwampPocket = function (pocketId) {
        return $http.delete("/api/pockets/" + pocketId);
    };
};

// Minification-safe dependency injection
PocketService.$inject = ["$q", "$http"];
SwampServices.service("PocketService", PocketService);
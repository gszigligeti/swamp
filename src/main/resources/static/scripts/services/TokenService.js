/**
 * Application service responsible for managing OAuth credentials
 * @author Gergely Szigligeti
 */
var TokenService = function ($http, $window, SwampConstants) {
    // Method for checking and saving a given access token to local storage
    this.saveCredentials = function (accessToken, refreshToken, tokenExpiration) {
        $window.localStorage.setItem("access_token", accessToken);
        $window.localStorage.setItem("refresh_token", refreshToken);
        $window.localStorage.setItem("token_expiration", Math.floor(Date.now() / 1000 + tokenExpiration));
    };

    // Method for retrieving the saved access token from local storage
    this.getAccessToken = function () {
        return $window.localStorage.getItem("access_token");
    };

    // Method for retrieving the saved refresh token from local storage
    this.getRefreshToken = function () {
        return $window.localStorage.getItem("refresh_token");
    };

    // Method for refreshing an expired access token
    this.refreshAccessToken = function () {
        var requestData = new Object();
        var refreshToken = this.getRefreshToken();

        this.removeCredentials();

        requestData.client_id = SwampConstants.oauth.clientId;
        requestData.grant_type = SwampConstants.oauth.refreshTokenGrantType;
        requestData.refresh_token = refreshToken;

        return $http.post("/api/authentication/token", requestData, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }).success(function (response) {
            this.saveCredentials(response.accessToken, response.refresh_token, response.expires_in);
        });
    };

    // Method for deciding whether the access token is expired or not
    this.isAccessTokenExpired = function () {
        var accessToken = $window.localStorage.getItem("access_token");
        var tokenExpiration = $window.localStorage.getItem("token_expiration");

        return !accessToken || !tokenExpiration || tokenExpiration < (Date.now() / 1000);
    };

    // Method for erasing token credentials from local storage
    this.removeCredentials = function () {
        $window.localStorage.removeItem("access_token");
        $window.localStorage.removeItem("refresh_token");
        $window.localStorage.removeItem("token_expiration");
    };
};

// Minification-safe dependency injection
TokenService.$inject = ["$http", "$window", "SwampConstants"];
SwampServices.service("TokenService", TokenService);
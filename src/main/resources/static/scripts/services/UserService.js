/**
 * Application service responsible for handling user-related operations
 * @author Gergely Szigligeti
 */
var UserService = function ($q, $http) {
    // Method for updating user account
    this.updateSwampUserAccount = function (updateDetails) {
        return $http.post("/api/users/" + updateDetails.userId, updateDetails);
    };

    // Method for fetching every visible user
    this.findEverySwampUser = function () {
        return $http.get("/api/users");
    };

    this.deleteSwampUserAccount = function (userId, password) {
        return $http.delete("/api/users/" + userId + "?password=" + password);
    };
};

// Minification-safe dependency injection
UserService.$inject = ["$q", "$http"];
SwampServices.service("UserService", UserService);